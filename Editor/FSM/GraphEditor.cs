﻿using System.Linq;
using Core.Editor.Extensions;
using Core.Editor.PopupWindows;
using Core.Extensions;
using ScriptableUtility.Behaviour;
using ScriptableUtility.FSM;
using UnityEditor;
using UnityEngine;
using nodegraph.Editor;
using nodegraph.Editor.Attributes;
using nodegraph.Editor.Graph;
using Event = UnityEngine.Event;

namespace ScriptableUtility.Editor.FSM
{

    [CustomNodeGraphEditor(typeof(FSMGraph))]
    public class FSMGraphEditor : NodeGraphEditor
    {
        public override void OnGUI()
        {
            base.OnGUI();
            if (Selection.activeObject is GameObject go && go.TryGetComponent(out FSMBehaviour fsmBehaviour))
                NodeEditorWindow.Current.ActiveNode = fsmBehaviour.GetActiveState();
            else NodeEditorWindow.Current.ActiveNode = null;
        }

        void AddNodeItems(GenericMenu menu, Vector2 mousePos, Vector2 gridPos)
        {
            menu.AddItem(new GUIContent("Add State"), false, () => EnterNameForStateNode(mousePos, gridPos));
            menu.AddItem(new GUIContent("Add Global Event"), false, () => EnterNameForEventNode(mousePos, gridPos));
        }

        public override void AddContextMenuItems(GenericMenu menu)
        {
            var mousePos = Event.current.mousePosition;
            var gridPos = NodeEditorWindow.Current.WindowToGridPosition(mousePos);
            AddNodeItems(menu, mousePos, gridPos);
            AddDefaultContextMenu(menu, gridPos);
        }

        void EnterNameForStateNode(Vector2 mousePos, Vector2 gridPos)
        {
            var popup = new NamePopup((name) => AddNode(gridPos, typeof(State), (string) name), checkName: CheckName);
            PopupWindow.Show(new Rect(mousePos, NamePopup.WindowSize), popup);
        }
        
        void EnterNameForEventNode(Vector2 mousePos, Vector2 gridPos)
        {
            string[] eventOptions = null;
            if (m_target is FSMGraph fsmGraph)
                eventOptions = fsmGraph.EventNames;

            var popup = new NamePopup((name) => AddNode(gridPos, typeof(GlobalEvent), (string) name), checkName: CheckName, options: eventOptions);
            PopupWindow.Show(new Rect(mousePos, NamePopup.WindowSize), popup);
        }

        bool CheckName(string name)
        {
            name = name.Trim();
            if (name.IsNullOrEmpty())
                return false;
            return m_target.NodeObjs.FirstOrDefault(so => string.Equals(so.name, name)) == null;
        }
    }
}
