using JetBrains.Annotations;
using ScriptableUtility.FSM;
using UnityEditor;
using UnityEngine;
using nodegraph;
using nodegraph.Editor;
using nodegraph.Editor.Attributes;
using nodegraph.Editor.Data;
using nodegraph.Editor.Graph;
using nodegraph.Editor.Interface;
using nodegraph.Editor.Utility;

namespace ScriptableUtility.Editor.FSM
{
    // todo 4: remove this Editor and add some interfaces to enable/ disable copy, paste etc
    [UsedImplicitly]
    [CustomNodeEditor(typeof(State))]
    public class StateEditor : NodeEditorBase<State>, INodeEditor
    {
        public SerializedObject SerializedObjectData => SerializedObject;
        public INode Target => m_target;
        INodeData NodeData => m_target.NodeData;

        protected override void OnCreate()
        {
            base.OnCreate();
            DynamicConnectorUpdater.I.InitDynamicPorts(this);
        }

        public void OnHeaderGUI() => this.DefaultHeader();
        public void OnBodyGUI() => this.DefaultBodyGUI(SerializedObject, m_target);
        public int GetWidth() => this.DefaultGetWidth(NodeData);
        public Color GetTint() => this.DefaultGetTint(NodeData);
        public NodeStyles GetStyles() => this.DefaultGetStyles();
        public string GetHeaderTooltip() => null;

        public void AddContextMenuItems(GenericMenu menu) 
        {
            var canRemove = true;

            var selectedNode = Selection.activeObject as INode;
            var isNode = selectedNode != null;

            // Actions if only one node is selected
            if (Selection.objects.Length == 1 && isNode)
            {
                menu.AddItem(new GUIContent("Move To Top"), false, () => NodeEditorWindow.Current.MoveNodeToTop(selectedNode));
                //menu.AddItem(new GUIContent("Rename"), false, NodeEditorWindow.Current.RenameSelectedNode);
                var graphEditor = NodeEditorCache.GetEditor(selectedNode.Graph, NodeEditorWindow.Current);
                canRemove = graphEditor.CanRemove(selectedNode);
            }

            if (canRemove)
                menu.AddItem(new GUIContent("Remove"), false, NodeEditorWindow.Current.RemoveSelectedNodes);
            else 
                menu.AddItem(new GUIContent("Remove"), false, null);

            // Custom sections if only one node is selected
            if (Selection.objects.Length != 1 || !isNode)
                return;

            menu.AddCustomContextMenuItems(selectedNode);
        }


        public void Rename(string newName) => this.DefaultRename(newName);
        public void OnRename() { }
        public void OnNodeUpdated(){}
    }
}