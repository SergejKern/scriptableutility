using System;
using System.Collections.Generic;
using System.Linq;
using Core.Editor.Inspector;
using Core.Extensions;
using Core.Unity.Extensions;
using ScriptableUtility.FSM;
using UnityEditor;
using UnityEngine;
using nodegraph.Editor;
using static Core.Editor.Utility.CustomGUI;

namespace ScriptableUtility.Editor.FSM
{
    [CustomEditor(typeof(FSMGraph), true)]
    [CanEditMultipleObjects]
    public class GraphInspector : BaseInspector<GraphInspectorEditor>
    {
        public override bool CanDetach => false;
    }

    public class GraphInspectorEditor : BaseEditor<FSMGraph>
    {
        const float k_smallElementWidth = 20f;
        const float k_usesWidth = 45f;
        const float k_defaultLabelWidth = 75f;
        const string k_newEventTextField = "NewEventTextField";

        public enum InspectorState
        {
            Selected,
            Events,
            Variables
        }

        public InspectorState Current { get; private set; }
        string m_newEvent;
        UnityEditor.Editor m_editor;

        readonly Dictionary<string, int> m_eventUsage = new Dictionary<string, int>();
        bool m_isExpanded;

        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);

            InitEventUsage();
        }

        void InitEventUsage()
        {
            foreach (var ev in Target.EventNames)
                m_eventUsage.Add(ev, 0);

            var states = Target.NodeObjs.OfType<State>();
            var events = Target.NodeObjs.OfType<GlobalEvent>();
            foreach (var s in states)
            {
                foreach (var e in s.Events)
                    m_eventUsage[e.Name]++;
            }

            foreach (var e in events)
                m_eventUsage[e.Name]++;
        }

        public override void OnGUI(float width)
        {
            EditGraphGUI();
            StateButtonsGUI();
            InspectorStatesGUI(width);
        }

        void InspectorStatesGUI(float width)
        {
            switch (Current)
            {
                case InspectorState.Selected:
                    SelectedGUI();
                    break;
                case InspectorState.Events:
                    EventsGUI(width);
                    break;
                case InspectorState.Variables:
                    VariablesGUI();
                    break;
            }
        }

        static void SelectedGUI()
        {
            if (Selection.activeObject is State || Selection.activeObject is GlobalEvent)
                return;

            EditorGUILayout.HelpBox("No state or event selected", MessageType.Info);
        }

        void StateButtonsGUI()
        {
            using (new GUILayout.HorizontalScope())
            {
                var enumNames = Enum.GetNames(typeof(InspectorState));
                for (var i = 0; i < enumNames.Length; i++)
                {
                    var state = (InspectorState) i;
                    var stateName = enumNames[i];

                    if (ActivityButton(Current == state, stateName))
                        Current = (InspectorState) i;
                }
            }
        }

        void EditGraphGUI()
        {
            if (NodeEditorWindow.Current != null && NodeEditorWindow.Current.IsVisible &&
                ReferenceEquals(NodeEditorWindow.Current.Graph, Target)) 
                return;

            if (!GUILayout.Button("Edit graph", EditorStyles.toolbarButton)) 
                return;

            var w = NodeEditorWindow.Open(Target);
            w.ResetView(); // Focus selected node
        }

        static void GetWidths(float width, out float nameWidth)
        {
            const float spacings = 7f;
            nameWidth = width - (k_smallElementWidth + k_usesWidth + spacings);
        }

        void EventsGUI(float width)
        {
            GetWidths(width, out var nameWidth);
            EventsGUITableHeader(nameWidth);
            ExistingEventsGUI(nameWidth);
            AddEventGUI();
        }

        void ExistingEventsGUI(float nameWidth)
        {
            var deleteIdx = -1;
            for (var i = 0; i < Target.EventNames.Length; i++)
            {
                using (new GUILayout.HorizontalScope())
                {
                    var @event = Target.EventNames[i];
                    GUILayout.Label(@event, GUILayout.Width(nameWidth));
                    // uses:
                    GUILayout.Label($"{m_eventUsage[@event]}", GUILayout.Width(k_usesWidth));
                    if (GUILayout.Button("X", GUILayout.Width(k_smallElementWidth)))
                        deleteIdx = i;
                }
            }

            RemoveEvent(deleteIdx);
        }

        void RemoveEvent(int deleteIdx)
        {
            if (deleteIdx == -1) 
                return;

            var eventName = Target.EventNames[deleteIdx];
            CollectionExtensions.RemoveAt(ref Target.Editor_Events, deleteIdx);
            Target.RemoveEvent(eventName);
        }

        void AddEventGUI()
        {
            GUILayout.Space(5f);
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("Add Event:", GUILayout.Width(k_defaultLabelWidth));
                GUI.SetNextControlName(k_newEventTextField);
                m_newEvent = GUILayout.TextField(m_newEvent);
            }
            GUI.FocusControl(k_newEventTextField);

            using (new EditorGUI.DisabledScope(!NewEventIsValid()))
            {
                if (!ButtonOrReturnKey("+")
                    || !NewEventIsValid())
                    return;

                AddNewEvent();
            }
        }

        static void EventsGUITableHeader(float nameWidth)
        {
            GUILayout.Label("Graph Events: ", EditorStyles.miniBoldLabel);
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("Event-Name", EditorStyles.miniLabel, GUILayout.Width(nameWidth));
                GUILayout.Label("Uses", EditorStyles.miniLabel, GUILayout.Width(k_usesWidth));
                GUILayout.Label("", EditorStyles.miniLabel, GUILayout.Width(k_smallElementWidth));
            }
        }

        void AddNewEvent()
        {
            CollectionExtensions.Add(ref Target.Editor_Events, m_newEvent);
            m_eventUsage.Add(m_newEvent, 0);
            m_newEvent = "";
        }

        bool NewEventIsValid()
        {
            if (m_newEvent.IsNullOrEmpty())
                return false;
            return !Target.EventNames.Contains(m_newEvent);
        }

        void VariablesGUI()
        {
            ref var container = ref Target.Editor_Container;
            if (container == null && GUILayout.Button("Create Nested"))
            {
                container = ScriptableObject.CreateInstance<ScriptableContainer>();
                container.name = "Variables";
                AssetDatabase.AddObjectToAsset(container, Target);
                AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(Target));
            }

            if (container != null && AssetDatabase.IsSubAsset(container))
            {
                if (GUILayout.Button("Remove Nested"))
                {
                    container.DestroyEx();
                    container = null;
                    AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(Target));
                }
            }
            else container = EditorGUILayout.ObjectField(container, typeof(ScriptableContainer), false) as ScriptableContainer;

            UnityEditor.Editor.CreateCachedEditor(container, typeof(ScriptableContainerEditor), ref m_editor);

            ScriptableContainerEditor();
        }

        void ScriptableContainerEditor()
        {
            var container = Target.Editor_Container;
            if (m_editor == null || container == null)
                return;

            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                m_isExpanded = EditorGUILayout.Foldout(m_isExpanded, container.name, true, EditorStyles.foldoutHeader);
                if (!m_isExpanded)
                    return;

                GUILayout.Label("Container Inspector: ");

                m_editor.OnInspectorGUI();
            }
        }
    }
}