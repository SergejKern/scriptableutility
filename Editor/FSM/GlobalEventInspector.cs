﻿using System;
using System.Linq;
using Core.Editor.Inspector;
using ScriptableUtility.FSM;
using UnityEditor;
using UnityEngine;

namespace ScriptableUtility.Editor.FSM
{
    [CustomEditor(typeof(GlobalEvent), true)]
    [CanEditMultipleObjects]
    public class GlobalEventInspector : BaseInspector<GlobalEventInspectorEditor>
    {
        public override bool CanDetach => false;
    }

    public class GlobalEventInspectorEditor : BaseEditor<GlobalEvent>
    {
        const float k_defaultLabelWidth = 75f;

        GraphInspectorEditor m_graphInspector;

        #region Init
        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);

            InitParentGraphInspector(parentContainer);
        }

        void InitParentGraphInspector(object parentContainer)
        {
            m_graphInspector = new GraphInspectorEditor() {TargetObj = (FSMGraph) Target.Graph};
            m_graphInspector.Init(parentContainer);
        }
        #endregion

        public override void Terminate()
        {
            m_graphInspector.Terminate();
            m_graphInspector = null;

            base.Terminate();
        }

        public override void OnGUI(float width)
        {
            m_graphInspector.OnGUI(width);
            GUILayout.Space(5f);

            if (m_graphInspector.Current == GraphInspectorEditor.InspectorState.Selected)
                GlobalEventGUI();
        }

        void GlobalEventGUI()
        {
            GUILayout.Label("Event: ", EditorStyles.miniBoldLabel, GUILayout.Width(k_defaultLabelWidth));

            NameGUI();
            InfoGUI();
        }

        void NameGUI()
        {
            var parent = (FSMGraph) Target.Graph;
            var events = parent.EventNames;

            var idx = Array.IndexOf(events, Target.Name);

            using (var check = new EditorGUI.ChangeCheckScope())
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("Name: ", GUILayout.Width(k_defaultLabelWidth));
                idx = EditorGUILayout.Popup(idx, events);

                if (!check.changed)
                    return;
                var exists = parent.NodeObjs.OfType<GlobalEvent>()
                    .FirstOrDefault(e => string.Equals(e.Name, events[idx])) != null;

                // todo 4: inform user
                if (exists)
                    return;
                Target.SetName(events[idx]);
            }
        }

        void InfoGUI()
        {
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("Info: ", GUILayout.Width(k_defaultLabelWidth));
                Target.Description = GUILayout.TextArea(Target.Description);
            }
        }
    }
}