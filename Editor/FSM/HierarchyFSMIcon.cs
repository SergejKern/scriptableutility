using System.Collections.Generic;
using System.Linq;
using ScriptableUtility.Behaviour;
using UnityEditor;

using UnityEditor.Experimental.SceneManagement;
using UnityEngine;

namespace ScriptableUtility.Editor.FSM
{
    // todo 3: make general solution for all kinds of types with icons
    [InitializeOnLoad]
    internal class HierarchyFSMIcon
    {
        static readonly Texture2D k_texture;
        static readonly List<int> k_markedObjects = new List<int>();

        static HierarchyFSMIcon()
        {
            k_texture = Resources.Load("Icons/icon-fsm",  typeof(Texture2D)) as Texture2D;

            EditorApplication.hierarchyChanged += UpdateObjects;
            EditorApplication.hierarchyWindowItemOnGUI += HierarchyItemOnGUI;
        }

        static void UpdateObjects()
        {
            k_markedObjects.Clear();

            AddComponentsInScene();
            AddComponentsInPrefabView();
        }

        static void AddComponentsInPrefabView()
        {
            var stage = PrefabStageUtility.GetCurrentPrefabStage();
            if (stage == null)
                return;

            var fsmBehaviour = stage.FindComponentsOfType<FSMBehaviour>();
            foreach (var fsm in fsmBehaviour)
                k_markedObjects.Add(fsm.gameObject.GetInstanceID());
        }

        static void AddComponentsInScene()
        {
            var fsmBehaviour = Object.FindObjectsOfType(typeof(FSMBehaviour)).OfType<FSMBehaviour>();
            foreach (var fsm in fsmBehaviour) 
                k_markedObjects.Add(fsm.gameObject.GetInstanceID());
        }

        static void HierarchyItemOnGUI(int instanceID, Rect selectionRect)
        {
            // place the icon to the right of the list:
            var r = new Rect(selectionRect);
            r.x = r.x + r.width - 20;
            r.width = 18;

            if (k_markedObjects.Contains(instanceID)) 
                GUI.Label(r, k_texture);
        }
    }
}