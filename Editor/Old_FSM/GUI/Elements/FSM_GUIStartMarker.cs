﻿using Core.Editor.Interface.GUI;
using Core.Editor.Utility.GUIStyles;
using ScriptableUtility.FSM.Old_FSM;
using UnityEngine;

namespace ScriptableUtility.Editor.Old_FSM.GUI.Elements
{
    public class FSM_GUIGlobalEventMarker : IGUIConnectable, IGUIWithText
    {
        static readonly Vector2 k_defaultSize = FSM_GUIStateNode.K_DefaultSize;

        FSM_GUIStateNode m_connectToState;
        public OldFSMConfig.EventData SerializedEventData;

        public Vector2 Pos => m_connectToState.Pos + (Size.x + 40) * Vector2.left;
        public Vector2 Size => k_defaultSize;
        public string Text => SerializedEventData.Event;
        public GUIStyle Style => SquareNodes.Black;

        public ConnectionPos IncomingPos => ConnectionPos.None;
        public ConnectionPos OutgoingPos => ConnectionPos.Right;

        public FSM_GUIGlobalEventMarker(OldFSMConfig.EventData data, FSM_GUIStateNode connectToNode)
        {
            SerializedEventData = data;
            m_connectToState = connectToNode;
        }

        public void SetConnectToState(FSM_GUIStateNode state)
        {
            m_connectToState = state;
        }
    }
}