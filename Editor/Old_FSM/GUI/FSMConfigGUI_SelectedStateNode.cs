using Core.Editor.Utility.NodeGUI;
using Core.Unity.Extensions;
using Core.Unity.Utility.GUITools;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.FSM.Old_FSM;
using UnityEditor;
using UnityEngine;

namespace ScriptableUtility.Editor.Old_FSM.GUI
{
    public partial class FSMConfigGUI
    {
        const float k_actionSelectionRectHeight = 15f;

        static readonly Color k_stateNodeGUIBgColor = new Color(0.75f,0.75f,0.75f,1);
        readonly bool m_isNested;
        // selection & editing:
        StateEditor m_selectedStateEditor;

        bool HasNestedGUI => m_selectedStateEditor.NestedFSM != null || m_selectedStateEditor.CachedEditor != null;
        void SelectedStateNodeGUI()
        {
            if (SelectedNode == null)
                return;

            ref var state = ref SelectedState;
            var nestedRect = SelectedNode.GetGUIElementRect(m_grid);
            // draw after the node
            nestedRect.x += nestedRect.width + 20;
            nestedRect.y -= 10;
            nestedRect.width = 400 * m_zoom;
            nestedRect.height = HasNestedGUI?400 * m_zoom: k_actionSelectionRectHeight;
            GUITools.DrawRect(nestedRect, k_stateNodeGUIBgColor);

            var actionSelectionRect = new Rect(nestedRect.x, nestedRect.y, nestedRect.width, k_actionSelectionRectHeight);
            state.Action = (ScriptableBaseAction) EditorGUI.ObjectField(actionSelectionRect, state.Action,
                typeof(ScriptableBaseAction),
                false);

            nestedRect.y += actionSelectionRect.height;
            nestedRect.height -= actionSelectionRect.height+1;
            nestedRect.x += 1;
            nestedRect.width -= 2;

            DrawNestedConfigGUI(nestedRect);
        }

        void DrawNestedConfigGUI(Rect drawRect)
        {
            if (!HasNestedGUI)
                return;

            if (m_selectedStateEditor.NestedFSM == null)
                return;

            m_selectedStateEditor.NestedFSM.SetGUIRects(drawRect, m_grid.EventRect);
            m_selectedStateEditor.NestedFSM.SetZoomFactor(m_zoom * 0.5f);
            m_selectedStateEditor.NestedFSM.OnGUI();
            //using (new GUILayout.AreaScope(drawRect))
            //{
            //    if (m_selectedStateEditor.CachedEditor != null)
            //    {
            //        m_selectedStateEditor.CachedEditor.OnInspectorGUI();
            //    }
            //}
        }
        void ProcessNestedConfigGUI() => m_selectedStateEditor.NestedFSM?.ProcessGUI();

        void UpdateSelectedState()
        {
            if (SelectedNode == null)
            {
                if (m_selectedStateEditor.Action != null)
                    ReleaseSelectedStateEditors();
                return;
            }
            ref var state = ref SelectedState;
            if (m_selectedStateEditor.Action != state.Action)
                ReleaseSelectedStateEditors();
            else return;

            m_selectedStateEditor.Action = state.Action;

            if (state.Action == null)
                return;
            if (state.Action is OldFSMConfig fsmConfig)
            {
                m_selectedStateEditor.NestedFSM = new FSMConfigGUI(fsmConfig, m_repaintable, true);
                return;
            }
            var editor = UnityEditor.Editor.CreateEditor(state.Action, null);
            m_selectedStateEditor.CachedEditor = editor;
        }

        void ReleaseSelectedStateEditors()
        {
            if (m_selectedStateEditor.CachedEditor != null)
                m_selectedStateEditor.CachedEditor.DestroyEx();
            m_selectedStateEditor = default;
        }
    }
}