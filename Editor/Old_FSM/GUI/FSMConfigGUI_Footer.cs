using Core.Editor.Utility;
using UnityEditor;
using UnityEngine;

namespace ScriptableUtility.Editor.Old_FSM.GUI
{
    public partial class FSMConfigGUI
    {
        const float k_footerHeight = 16f;
        Rect m_footerRect;

        void InitFooterRect(Rect drawRect) => 
            m_footerRect = new Rect(0, drawRect.height - k_footerHeight, drawRect.width, k_footerHeight);

        void FooterGUI()
        {
            if (m_isNested)
                return;

            using (new GUILayout.AreaScope(m_footerRect, "", EditorStyles.toolbar))
            {
                using (new GUILayout.HorizontalScope())
                {
                    using (new GUILayout.HorizontalScope(GUILayout.Width(m_grid.DrawRect.width - 14)))
                    {
                        var targetPath = AssetDatabase.GetAssetPath(Target);
                        GUILayout.Label(targetPath);
                    }

                    CustomGUI.VSplitter();
                    using (new GUILayout.HorizontalScope(GUILayout.Width(m_sideBarWidth)))
                    {
                        GUILayout.Label("Footer2");
                    }
                }
            }
        }
    }
}