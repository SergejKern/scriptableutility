using ScriptableUtility.ActionConfigs;

namespace ScriptableUtility.Editor.Old_FSM.GUI
{
    public struct StateEditor
    {
        public ScriptableBaseAction Action;
        public UnityEditor.Editor CachedEditor;
        public FSMConfigGUI NestedFSM;
    }
}
