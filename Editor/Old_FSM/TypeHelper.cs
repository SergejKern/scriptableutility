﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Editor.Extensions;
using ScriptableUtility.ActionConfigs;
using UnityEditor;
using UnityEngine;

namespace ScriptableUtility.Editor.Obsolete
{
    public static class TypeHelper
    {
        static readonly Dictionary<Type, string> k_friendlyNames = new Dictionary<Type, string>();
        static readonly Type[] k_supportedDefaultTypes;

        static TypeHelper()
        {
            k_friendlyNames.Clear();
            k_friendlyNames.Add(typeof(void), "void");
            k_friendlyNames.Add(typeof(bool), "bool");
            k_friendlyNames.Add(typeof(int), "int");
            k_friendlyNames.Add(typeof(float), "float");
            k_friendlyNames.Add(typeof(string), "string");

            k_supportedDefaultTypes = new[]
            {
                typeof(string),
                typeof(int),
                typeof(float),
                typeof(bool),
                typeof(Vector2),
                typeof(Vector3),
                typeof(Color),
                typeof(Quaternion),
                typeof(Rect),
                typeof(Material),
                typeof(Texture),
            };
        }

        public static GenericMenu InitActionTypeMenuAll(GenericMenu.MenuFunction2 selectionFunction)
        {
            return InitActionTypeMenu(selectionFunction, null);
        }
        public static GenericMenu InitActionTypeMenuDefaultOnly(GenericMenu.MenuFunction2 selectionFunction)
        {
            return InitActionTypeMenu(selectionFunction, typeof(IDefaultAction));
        }
        static GenericMenu InitActionTypeMenu(GenericMenu.MenuFunction2 selectionFunction, Type filterType)
        {
            var scriptableActionTypes = new List<Type>();
            EditorExtensions.ReflectionGetAllTypes(typeof(ScriptableBaseAction), ref scriptableActionTypes,
                out var names, out var namespaces);

            var root = $"{nameof(ScriptableUtility)}.{nameof(Actions)}.";

            if (filterType != null)
            {
                Filter(scriptableActionTypes, ref names, ref namespaces, filterType);
            }

            return EditorExtensions.CreateGenericMenuFromTypes(scriptableActionTypes, names, namespaces, root, selectionFunction);
        }

        static void Filter(IList<Type> factoryTypes, ref string[] names, ref string[] nameSpaces, Type filterType)
        {
            var nameList = names.ToList();
            var nameSpacesList = nameSpaces.ToList();

            for (var i = factoryTypes.Count - 1; i >= 0; --i)
            {
                var actionFac = factoryTypes[i];
                var prop = actionFac.GetProperty("StaticFactoryType");
                var facType = prop?.GetValue(null) as Type;
                if (filterType.IsAssignableFrom(facType))
                    continue;
                factoryTypes.RemoveAt(i);
                nameList.RemoveAt(i);
                nameSpacesList.RemoveAt(i);
            }

            names = nameList.ToArray();
            nameSpaces = nameSpacesList.ToArray();
        }

        public static GenericMenu GenerateMethodMenu(Type type,
            GenericMenu.MenuFunction2 selectionFunction)
        {
            return GenerateMethodMenu(type, selectionFunction, BindingFlags.Instance | BindingFlags.Public);
        }

        static GenericMenu GenerateMethodMenu(Type type,
            GenericMenu.MenuFunction2 selectionFunction,
            BindingFlags bindingFlags)
        {
            var genericMenu = new GenericMenu();
            if (type == null)
                return genericMenu;
            foreach (var method in type.GetMethods(bindingFlags))
            {
                if (!IsValidMethod(method))
                    continue;

                var text = GetMethodSignature(method);
                if (IsInherited(method))
                    text = "Inherited/" + text;

                genericMenu.AddItem(new GUIContent(text), false, selectionFunction, method);
            }
            return genericMenu;
        }

        static string GetMethodSignature(MethodInfo method)
        {
            var str = GetFriendlyName(method.ReturnType) + " " + method.Name + " (";
            var flag = true;
            foreach (var parameter in method.GetParameters())
            {
                if (!flag)
                    str += ", ";
                str += GetFriendlyName(parameter.ParameterType);
                flag = false;
            }
            return str + ")";
        }

        static string GetFriendlyName(Type t)
        {
            if (t == null)
                return "";
            if (t.IsArray)
                return GetFriendlyName(t.GetElementType()) + "[]";
            return !k_friendlyNames.TryGetValue(t, out var str) ? t.Name : str;
        }

        static bool IsValidMethod(MethodInfo method)
        {
            if (method.IsGenericMethod ||
                method.IsSpecialName ||
                method.ReturnType != typeof(void) &&
                !IsSupportedParameterType(method.ReturnType))
                return false;

            return method.GetParameters().All(parameter => IsSupportedParameterType(parameter.ParameterType));
        }

        static bool IsInherited(MethodInfo method)
        {
            var declaringType = method.DeclaringType;
            return (declaringType == typeof(MonoBehaviour)
                    || declaringType == typeof(Component)
                    || declaringType == typeof(UnityEngine.Behaviour)
                    || declaringType == typeof(UnityEngine.Object));
        }

        static bool IsSupportedParameterType(Type parameterType)
        {
            for (; parameterType != null; parameterType = parameterType.GetElementType())
            {
                if (parameterType.IsArray)
                    continue;
                if (Array.IndexOf(k_supportedDefaultTypes, parameterType) != -1)
                    return true;

                return parameterType.IsEnum
                       || parameterType.IsSubclassOf(typeof(UnityEngine.Object));
            }
            return false;
        }
    }
}
