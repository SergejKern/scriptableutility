﻿using Core.Editor.Interface.GUI;
using ScriptableUtility.FSM.Old_FSM;
using UnityEditor;
using UnityEngine;
using FSMConfigGUI = ScriptableUtility.Editor.Old_FSM.GUI.FSMConfigGUI;

namespace ScriptableUtility.Editor.Old_FSM
{
    public class FSMConfigWindow : EditorWindow, IGUIRepaintable
    {
        FSMConfigGUI m_gui;

        OldFSMConfig Target
        {
            get => m_gui.Target;
            set
            {
                if (m_gui != null && m_gui.Target == value)
                    return;
                m_gui = value != null ? new FSMConfigGUI(value, this) : null;
            }
        }

        [MenuItem("Tools/ScriptableUtility/FSM Config Window")]
        public static void OpenWindow()
        {
            var confEditor = (FSMConfigWindow) GetWindow(typeof(FSMConfigWindow));
            confEditor.titleContent = new GUIContent("FSM Config Window");
            confEditor.Show();
        }

        void OnEnable()
        {
            m_gui = null;
            UpdateSelectedTarget();
        }

        void OnGUI()
        {
            UpdateSelectedTarget();
            
            Draw_FSM_GUI();
            No_FSM_GUI();
        }

        void Draw_FSM_GUI()
        {
            if (m_gui == null)
                return;

            var drawRect = new Rect(0, 0, position.width, position.height);
            m_gui.SetGUIRects(drawRect, drawRect);
            m_gui.OnGUI();
            m_gui.ProcessGUI();
        }

        void No_FSM_GUI()
        {
            if (m_gui != null)
                return;
            EditorGUILayout.HelpBox("No FSMConfig selected!", MessageType.Info);
        }

        void UpdateSelectedTarget()
        {
            if (Selection.activeGameObject != null)
            {
                // todo 1 FSM 3: check if FSMConfig inside
                // maybe also use as Selection.activeContext and check all variables exist
            }

            if (Selection.activeObject != null)
            {
                var fsmConfig = Selection.activeObject as OldFSMConfig;
                if (fsmConfig != null)
                    Target = fsmConfig;
            }
        }
    }
}