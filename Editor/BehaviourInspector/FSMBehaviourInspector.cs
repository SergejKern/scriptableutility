﻿using System.Linq;
using Core.Editor.Inspector;
using ScriptableUtility.Behaviour;
using ScriptableUtility.FSM;
using UnityEditor;
using UnityEngine;

namespace ScriptableUtility.Editor.BehaviourInspector
{
    [CustomEditor(typeof(FSMBehaviour))]
    public class FSMBehaviourInspector : BaseInspector<FSMBehaviourEditor>{}

    public class FSMBehaviourEditor : BaseEditor<FSMBehaviour>
    {
        public override void OnGUI(float width)
        {
            base.OnGUI(width);
            if (EditorApplication.isPlaying) 
                PlayingGUI();
        }

        void PlayingGUI()
        {
            GUILayout.Label("States: ", EditorStyles.miniBoldLabel);
            var states = Target.Graph.NodeObjs.OfType<State>();
            foreach (var state in states)
            {
                if (GUILayout.Button(state.Name))
                    Target.SetState(state.Name);
            }
            GUILayout.Label("Events: ", EditorStyles.miniBoldLabel);
            var events = Target.Graph.EventNames;
            foreach (var ev in events)
            {
                if (GUILayout.Button(ev))
                    Target.SendEvent(ev);
            }
        }
    }
}