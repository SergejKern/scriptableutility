﻿using System;
using Core.Types;
using ScriptableUtility.ActionConfigs.ScriptControl;
using nodegraph.Editor.Attributes;
using nodegraph.Editor.Graph;

namespace ScriptableUtility.Editor.Actions.ScriptControl
{
    [CustomNodeEditor(typeof(CallMethod))]
    public class CallMethodNode : NodeEditor
    {
        new CallMethod Target => base.Target as CallMethod;
        protected override void OnCreate()
        {
            base.OnCreate();
            if (Target.Behaviour.Type == null || string.IsNullOrEmpty(Target.MethodName))
                return;

            var info = Target.Behaviour.Type.GetMethod(Target.MethodName, Target.GetParamTypes());
            if (info == null)
                return;
            CallMethodEditor.GetParameterTypesAndNames(info, out var returnType, out var parameterTypes,
                out var parameterNames);
            if (CallMethodEditor.TypesToVarType(returnType, parameterTypes,
                out var returnVarType, out var parameterVarTypes, out _) == OperationResult.Error)
                return;
            CallMethodEditor.ToDynamicPorts(Target, returnVarType, parameterVarTypes, parameterNames);
        }
    }
}
