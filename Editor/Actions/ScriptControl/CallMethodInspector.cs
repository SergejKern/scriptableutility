﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Core.Editor.Inspector;
using Core.Extensions;
using Core.Types;
using ScriptableUtility.ActionConfigs.ScriptControl;
using UnityEditor;
using UnityEngine;
using nodegraph.Editor.Operations;

namespace ScriptableUtility.Editor.Actions.ScriptControl
{
    [CustomEditor(typeof(CallMethod))]
    public class CallMethodInspector : BaseInspector<CallMethodEditor>
    {
        public override bool CanDetach => false;
    }

    public class CallMethodEditor : BaseEditor<CallMethod>
    {
        string m_lastError;
        
        MethodInfo[] m_cachedMethods;
        string[] m_cachedMethodNames;

        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);
            CacheMethods();
            CacheParameterOfCurrentMethod();
        }

        public override void OnGUI(float width)
        {
            //base.OnGUI(width);
            SetBehaviourGUI();
            SelectMethodGUI();

            if (!m_lastError.IsNullOrEmpty()) 
                EditorGUILayout.HelpBox(m_lastError, MessageType.Error);
        }

        void SetBehaviourGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(SerializedObject.FindProperty(nameof(CallMethod.Behaviour)));
                if (!check.changed) 
                    return;

                SerializedObject.ApplyModifiedProperties();
                CacheMethods();
                ClearPreviousMethodData();

                OnChanged();
            }
        }

        void SelectMethodGUI()
        {
            GUILayout.Label(Target.Editor_MethodSig);

            if (m_cachedMethodNames == null) 
                return;

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var idx = Array.IndexOf(m_cachedMethodNames, Target.Editor_MethodSig);
                idx = EditorGUILayout.Popup(idx, m_cachedMethodNames);

                if (!check.changed) 
                    return;

                // if we cannot make VarReference<T> types out of method parameter, abort
                if (CacheParameter(m_cachedMethods[idx], out var returnVarType, out var parameterVarTypes, 
                    out var paramTypes, out var parameterNames) == OperationResult.Error)
                    return;

                m_lastError = "";

                Target.ClearDynamicPorts();
                ToDynamicPorts(returnVarType, parameterVarTypes, parameterNames);
                Target.Editor_SetParameter(paramTypes, parameterNames);

                if (idx != -1)
                {
                    var methodSignature = m_cachedMethodNames[idx];
                    var paramIdx = methodSignature.IndexOf('(');
                    Target.MethodName = methodSignature.Substring(0, paramIdx);
                }
                else Target.MethodName = "";

                OnChanged();
            }
        }

        void ClearPreviousMethodData()
        {
            m_lastError = "";
            Target.ClearDynamicPorts();
            Target.Editor_SetParameter(null, null);
            Target.MethodName = "";
        }

        void CacheMethods()
        {
            m_cachedMethods = null;
            m_cachedMethodNames = null;
            if (Target.Behaviour.Type == null)
                return;

            // todo 2: make popup out of this with path-tree for inherited, different binding flags etc...
            m_cachedMethods = Target.Behaviour.Type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            m_cachedMethodNames = new string[m_cachedMethods.Length];
            for (var i = 0; i < m_cachedMethods.Length; i++)
            {
                var argStr = GetParameterArgSignature(m_cachedMethods[i]);
                m_cachedMethodNames[i] = $"{m_cachedMethods[i].Name}({argStr})";
            }

        }

        static string GetParameterArgSignature(MethodInfo method)
        {
            var @params = method.GetParameters();
            var argStr = "";

            for (var j = 0; j < @params.Length; j++)
            {
                var separator = j == 0 ? "" : ",";
                argStr += $"{argStr}{separator}{@params[j].ParameterType.Name}";
            }

            return argStr;
        }

        public static void GetParameterTypesAndNames(MethodInfo methodInfo, out Type returnType, 
            out Type[] parameterTypes, out string[] parameterNames)
        {
            returnType = methodInfo.ReturnParameter?.ParameterType;
            if (returnType == typeof(void))
                returnType = null;

            var parameter = methodInfo.GetParameters();
            parameterNames = new string[parameter.Length];
            parameterTypes = new Type[parameter.Length];
            for (var i = 0; i < parameter.Length; i++)
            {
                parameterNames[i] = parameter[i].Name;
                parameterTypes[i] = parameter[i].ParameterType;
            }
        }


        void CacheParameterOfCurrentMethod()
        {
            if (m_cachedMethodNames == null 
                || m_cachedMethods == null) 
                return;

            var idx = Array.IndexOf(m_cachedMethodNames, Target.Editor_MethodSig);
            if (idx == -1)
                return;
            if (CacheParameter(m_cachedMethods[idx], out _, out _,
                out var paramTypes, out var paramNames) == OperationResult.OK)
                Target.Editor_SetParameter(paramTypes, paramNames);
        }

        OperationResult CacheParameter(MethodInfo methodInfo, out Type returnVarType, out Type[] parameterVarTypes, 
            out Type[] parameterTypes, out string[] parameterNames)
        {
            GetParameterTypesAndNames(methodInfo, out var returnType, out parameterTypes, out parameterNames);
            // SerializeParameters(returnType, parameterTypes);

            return TypesToVarType(returnType, parameterTypes,
                out returnVarType, out parameterVarTypes,
                out m_lastError);
        }

        public static OperationResult TypesToVarType(Type returnType, IReadOnlyList<Type> parameterTypes, 
            out Type returnVarType, out Type[] parameterVarTypes, out string error)
        {
            var varGenericType = typeof(VarReference<>);

            try
            {
                returnVarType = returnType != null 
                    ? varGenericType.MakeGenericType(returnType) 
                    : null;

                parameterVarTypes = new Type[parameterTypes.Count];
                for (var i = 0; i < parameterTypes.Count; i++)
                {
                    var parameterType = parameterTypes[i];
                    parameterVarTypes[i] = varGenericType.MakeGenericType(parameterType);
                }

                error = "";
                return OperationResult.OK;
            }
            catch (Exception exception)
            {
                error = exception.Message;
                returnVarType = null;
                parameterVarTypes = null;
                return OperationResult.Error;
            }
        }

        void ToDynamicPorts(Type returnType, IReadOnlyList<Type> parameterTypes, IReadOnlyList<string> parameterNames)
            => ToDynamicPorts(Target, returnType, parameterTypes, parameterNames);

        public static void ToDynamicPorts(CallMethod target, Type returnType, IReadOnlyList<Type> parameterTypes, IReadOnlyList<string> parameterNames)
        {
            if (returnType != null && !target.HasPort(CallMethod.ReturnName)) 
                target.AddDynamicInput(returnType, fieldName: CallMethod.ReturnName);
            for (var i = 0; i < parameterTypes.Count; i++)
            {
                if (!target.HasPort(parameterNames[i]))
                    target.AddDynamicInput(parameterTypes[i], fieldName: parameterNames[i]);
            }
        }

        //void SerializeParameters(Type returnType, IReadOnlyList<Type> parameterTypes)
        //{
        //    var serializedReturnType = new SerializableType(returnType);
        //    var serializedParams = new SerializableType[parameterTypes.Count];
        //    for (var i = 0; i < parameterTypes.Count; i++)
        //        serializedParams[i] = new SerializableType(parameterTypes[i]);
        //    Target.
        //}
    }
}