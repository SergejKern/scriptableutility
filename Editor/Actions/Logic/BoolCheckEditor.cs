﻿using Core.Editor.Inspector;
using Core.Editor.Interface;
using ScriptableUtility.ActionConfigs.Logic;
using UnityEditor;

namespace ScriptableUtility.Editor.Actions.Logic
{
    [CustomEditor(typeof(BoolCheck))]
    public class BoolCheckInspector : BaseInspector<BoolCheckEditor>{}
    public class BoolCheckEditor : ScriptableBaseActionEditor<BoolCheck>
    {
        public override string Name => "Bool Check";

        enum ContextActionType
        {
            True,
            False
        }

        ContextActionType m_actionContext;

        CommonActionEditorGUI.ActionData m_trueActionData;
        CommonActionEditorGUI.ActionData m_falseActionData;

        CommonActionEditorGUI.ActionMenuData m_menuData;

        public override void Terminate()
        {
            base.Terminate();

            ReleaseEditor(ref m_trueActionData);
            ReleaseEditor(ref m_falseActionData);
        }

        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);

            InitActionDataDefault(out m_trueActionData, "True", nameof(BoolCheck.TrueAction));
            InitActionDataDefault(out m_falseActionData, "False", nameof(BoolCheck.FalseAction));

            CreateMenu(ref m_menuData, (data) => 
                OnTypeSelected(m_actionContext == ContextActionType.True 
                    ? m_trueActionData 
                    : m_falseActionData, 
                data));
        }

        public override void OnGUI(float width)
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var boolProp = SerializedObject.FindProperty(nameof(Target.BoolReference));
                var breakProp = SerializedObject.FindProperty(nameof(Target.BreakReference));

                var finishRule = SerializedObject.FindProperty(nameof(Target.FinishRule));
                var waitForBoolChange = SerializedObject.FindProperty(nameof(Target.WaitForBoolChange));
                var dontUpdateBoolCheck = SerializedObject.FindProperty(nameof(Target.DontUpdateBoolCheck));

                var breakVal = SerializedObject.FindProperty(nameof(Target.BreakValue));

                EditorGUILayout.PropertyField(boolProp);
                EditorGUILayout.PropertyField(breakProp);

                EditorGUILayout.PropertyField(finishRule);
                EditorGUILayout.PropertyField(breakVal);
                EditorGUILayout.PropertyField(waitForBoolChange);
                EditorGUILayout.PropertyField(dontUpdateBoolCheck);

                if (check.changed)
                {
                    SerializedObject.ApplyModifiedProperties();
                    OnChanged();
                }
            }

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                DefaultActionGUI(ref m_trueActionData, ref m_menuData,() => m_actionContext = ContextActionType.True );
                DefaultActionGUI(ref m_falseActionData, ref m_menuData, () => m_actionContext = ContextActionType.False);
                if (check.changed)
                    OnChanged();
            }
            VerificationGUI();
        }
    }
}
