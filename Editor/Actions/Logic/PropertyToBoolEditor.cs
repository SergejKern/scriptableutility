﻿using System;
using Core.Editor.Inspector;
using ScriptableUtility.ActionConfigs.Logic;
using UnityEditor;
using UnityEngine;

namespace ScriptableUtility.Editor.Actions.Logic
{
    [CustomEditor(typeof(PropertyToBool))]
    public class PropertyToBoolInspector : BaseInspector<PropertyToBoolEditor>{}

    public class PropertyToBoolEditor : BaseEditor<PropertyToBool>
    {
        string[] m_properties;

        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);

            GetProperties();
        }

        public override void OnGUI(float width)
        {
            var behaviourProp = SerializedObject.FindProperty(nameof(Target.MBehaviour));
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(behaviourProp);
                if (check.changed)
                {
                    SerializedObject.ApplyModifiedProperties();
                    GetProperties();
                    return;
                }
            }
            if (m_properties == null)
                return;

            var idx = Array.IndexOf(m_properties, Target.MPropertyName);
            var newIdx = EditorGUILayout.Popup(idx, m_properties);
            if (idx != newIdx)
            {
                Target.MPropertyName = m_properties[newIdx];
            }
            var gameObjectVarProp = SerializedObject.FindProperty(nameof(Target.MGameObjectRef));
            EditorGUILayout.PropertyField(gameObjectVarProp);

            var boolProp = SerializedObject.FindProperty(nameof(Target.MBoolRef));
            EditorGUILayout.PropertyField(boolProp);
        }

        void GetProperties()
        {
            var behaviour = Target.MBehaviour;
            if (behaviour == null)
                return;

            var type = behaviour.Type;
            if (type == null)
                return;
            if (!type.IsClass)
                return;
            if (!typeof(MonoBehaviour).IsAssignableFrom(type))
                return;

            var props = behaviour.Type.GetProperties();
            m_properties = new string[props.Length];
            for (var i = 0; i < props.Length; i++)
                m_properties[i] = props[i].Name;
        }
    }
}
