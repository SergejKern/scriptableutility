﻿{0}

namespace {1}
{{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/{2}", fileName = nameof({2}Variable))]
    public class {2}Variable : ScriptableVariable<{3}>, IOverrideMapping
    {{
        public void AddContext(IContext context, OverrideValue value) => AddContext(context, ({3}) value.ObjectValue);
    }}
}}