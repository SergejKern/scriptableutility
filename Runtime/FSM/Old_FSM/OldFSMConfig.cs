﻿using System;
using System.Collections.Generic;
using Core.Unity.Attributes;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions.FSM;
using UnityEngine;

namespace ScriptableUtility.FSM.Old_FSM
{
    //    [EditorIcon("icon-fsm")]
    //    [CreateAssetMenu(menuName = "Scriptable/Old_FSMConfig")]
    public class OldFSMConfig : ScriptableBaseAction //, ILinkActionConfigs
    {
        [Serializable]
        public struct State
        {
            public Vector2Int LastGraphPos;
            public string Name;
            public ScriptableBaseAction Action;
            public List<EventData> Connections;
        }
        [Serializable]
        public struct EventData
        {
            public string Event;
            public ScriptableBaseAction Condition;
            public int StateIdxTo;
        }
        public State[] States = new State[0];
        public EventData[] GlobalEvents = new EventData[0];
        public override string Name => $"{nameof(OldFSMConfig)}";
        static Type StaticFactoryType => typeof(FSMAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            //            var actionStates = new List<FSMAction.State>();
            //            foreach (var s in States)
            //                CreateActionState(s, actionStates);
            //            var globalEvents = new List<FSMAction.EventData>();
            //            foreach (var ge in GlobalEvents)
            //                CreateActionEvent(ge, globalEvents);
            return null;
            //            //return new FSMAction(actionStates, globalEvents);
        }
        //        public void LinkActions(Dictionary<IActionConfig, IBaseAction> graphActions)
        //        {
        //            throw new NotImplementedException();
        //        }
        //        static void CreateActionState(State s, ICollection<FSMAction.State> actionStates)
        //        {
        //            var action = s.Action.CreateAction();
        //            var stateConnections = new List<FSMAction.EventData>();
        //            foreach (var c in s.Connections)
        //                CreateActionEvent(c, stateConnections);
        //            //actionStates.Add(new FSMAction.State() {Name = s.Name, Action = action, Events = stateConnections});
        //        }
        //        static void CreateActionEvent(EventData data, ICollection<FSMAction.EventData> globalEvents)
        //        {
        //            var boolCondition = data.Condition.CreateAction() as IBoolAction;
        //            globalEvents.Add(new FSMAction.EventData()
        //            {
        //                Event = data.Event,
        //                Condition = boolCondition,
        //                StateIdxTo = data.StateIdxTo
        //            });
        //        }
        //        public IEnumerable<IActionConfig> GetActionConfigsLinked()
        //        {
        //            throw new NotImplementedException();
        //        }
    }
}