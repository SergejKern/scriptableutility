﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Interface;
using Core.Types;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using ScriptableUtility.Actions.FSM;
using UnityEngine;
using nodegraph;
using nodegraph.Attributes;

namespace ScriptableUtility.FSM
{
    [EditorIcon("icon-fsm")]
    [CreateAssetMenu(menuName = "Scriptable/FSM Graph", fileName = "FSM Graph")]
    [RequireNode(typeof(FSMGraphEntryNode))]
    public class FSMGraph : NodeGraph, IFSMConfig, IActionConfig, IProvider<IContext>, IVariableContainer, IParentMarker
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] string[] m_events = new string[0];

        [SerializeField] ScriptableContainer m_container;
#pragma warning restore 0649 // wrong warnings for SerializeField

        IContext m_context;

        public string[] EventNames => m_events;
        public string[] StateNames => NodeObjs.OfType<State>().Select(s => s.name).ToArray();

#if UNITY_EDITOR
        //public static string Editor_EventsPropName => nameof(m_events);
        public ref string[] Editor_Events => ref m_events;
        public ref ScriptableContainer Editor_Container => ref m_container;
#endif

        public void Get(out IContext provided) => provided = m_context;

        public IEnumerable<IScriptableVariable> Variables => m_container == null ? null : m_container.Variables;
        public List<ScriptableObject> VariableObjs => m_container == null ? null : m_container.VariableObjs;

        public string Name => name;
        static Type StaticFactoryType => typeof(FSMAction);
        public Type FactoryType => StaticFactoryType;

        public IBaseAction CreateAction()
        {
            using (var stateNodesScoped = SimplePool<List<State>>.I.GetScoped())
            using (var globalEventNodesScoped = SimplePool<List<GlobalEvent>>.I.GetScoped())
            {
                var stateNodes = stateNodesScoped.Obj;
                var globalEventNodes = globalEventNodesScoped.Obj;

                stateNodes.AddRange(NodeObjs.OfType<State>());
                globalEventNodes.AddRange(NodeObjs.OfType<GlobalEvent>());

                var states = CreateActionStates(stateNodes);
                var globalEvents = CreateGlobalEvents(globalEventNodes, stateNodes);

                return new FSMAction(states, globalEvents);
            }
        }

        #region Create Action Utility
        static FSMAction.EventData[] CreateGlobalEvents(IReadOnlyList<GlobalEvent> globalEventNodes, 
            IList<State> stateNodes)
        {
            var globalEvents = new FSMAction.EventData[globalEventNodes.Count];
            for (var i = 0; i < globalEventNodes.Count; i++)
            {
                var globalEvent = globalEventNodes[i];
                globalEvents[i] = CreateStateEventData(stateNodes, globalEvent.EventData);
            }

            return globalEvents;
        }

        static FSMAction.State[] CreateActionStates(IList<State> stateNodes)
        {
            var states = new FSMAction.State[stateNodes.Count];
            for (var i = 0; i < states.Length; i++) 
                states[i] = CreateState(stateNodes, stateNodes[i]);

            return states;
        }

        static FSMAction.State CreateState(IList<State> stateNodes, State stateNode)
        {
            Debug.Assert(stateNode.FSMStateAction != null);
            return new FSMAction.State()
            {
                Action = stateNode.FSMStateAction.CreateAction() as StateAction,
                Events = CreateStateEventData(stateNodes, stateNode),
                Name = stateNode.Name
            };
        }

        static FSMAction.EventData[] CreateStateEventData(IList<State> stateNodes, State stateNode)
        {
            var eventCount = stateNode.Events.Count;
            var events = new FSMAction.EventData[eventCount];
            for (var i = 0; i < eventCount; i++)
                events[i] = CreateStateEventData(stateNodes, stateNode.Events[i]);
            return events;
        }

        static FSMAction.EventData CreateStateEventData(IList<State> stateNodes, Event stateNodeEvent)
        {
            IBoolAction condition = null;
            if (stateNodeEvent.Condition != null)
                condition = stateNodeEvent.Condition.CreateAction() as IBoolAction;
            var eventData = new FSMAction.EventData()
            {
                Event = stateNodeEvent.Name,
                Condition = condition,
                StateIdxTo = stateNodes.IndexOf(stateNodeEvent.TargetState)
            };
            return eventData;
        }
        #endregion

        public void SetContext(IContext context) => m_context = context;

        public override IEnumerable<ScriptableObject> Dependencies
        {
            get
            {
                foreach (var d in base.Dependencies)
                    yield return d;
                yield return m_container;
            }
        }

        public IScriptableObject Parent => this;
    }
}
