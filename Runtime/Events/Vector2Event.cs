using Core.Unity.Attributes;
using UnityEngine;


namespace ScriptableUtility.Events
{
    [EditorIcon("icon-event")]
    [CreateAssetMenu(menuName = "Scriptable/Events/Vector2")]
    public class Vector2Event : ScriptableEvent<Vector2> {}
}
