using Core.Unity.Attributes;
using UnityEngine;


namespace ScriptableUtility.Events
{
    [EditorIcon("icon-event")]
    [CreateAssetMenu(menuName = "Scriptable/Events/AudioClip")]
    public class AudioClipEvent : ScriptableEvent<AudioClip> {}
}
