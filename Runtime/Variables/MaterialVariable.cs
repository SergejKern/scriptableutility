using Core.Unity.Attributes;
using UnityEngine;


namespace ScriptableUtility.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/Material", fileName = nameof(MaterialVariable))]
    public class MaterialVariable : ScriptableVariable<Material>, IOverrideMapping
    {
        public void AddContext(IContext context, OverrideValue value) => AddContext(context, (Material) value.ObjectValue);
    }
}
