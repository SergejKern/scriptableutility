using Core.Unity.Attributes;
using UnityEngine;


namespace ScriptableUtility.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/Object", fileName = nameof(ObjectVariable))]
    public class ObjectVariable : ScriptableVariable<Object>, IOverrideMapping
    {
        public void AddContext(IContext context, OverrideValue value) => AddContext(context, (Object) value.ObjectValue);
    }
}
