using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility.OverrideValueConverter;


namespace ScriptableUtility.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/Vector2", fileName = nameof(Vector2Variable))]
    public class Vector2Variable : ScriptableVariable<Vector2>, IOverrideMapping
    {
        public void AddContext(IContext context, OverrideValue value) 
            => AddContext(context, Vector2OverrideValueConverter.Get(value));
    }
}
