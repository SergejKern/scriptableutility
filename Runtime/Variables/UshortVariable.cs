using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility.OverrideValueConverter;


namespace ScriptableUtility.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/Ushort", fileName = nameof(UshortVariable))]
    public class UshortVariable : ScriptableVariable<ushort>, IOverrideMapping
    {
        public void AddContext(IContext context, OverrideValue value) 
            => AddContext(context, UShortOverrideValueConverter.Get(value));
    }
}
