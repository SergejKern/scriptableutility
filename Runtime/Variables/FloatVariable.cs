using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility.OverrideValueConverter;


namespace ScriptableUtility.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/Float", fileName = nameof(FloatVariable))]
    public class FloatVariable : ScriptableVariable<float>, IOverrideMapping
    {
        public void AddContext(IContext context, OverrideValue value) 
            => AddContext(context, FloatOverrideValueConverter.Get(value));
    }
}
