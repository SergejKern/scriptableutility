using Core.Unity.Attributes;
using UnityEngine;


namespace ScriptableUtility.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/Sprite", fileName = nameof(SpriteVariable))]
    public class SpriteVariable : ScriptableVariable<Sprite>, IOverrideMapping
    {
        public void AddContext(IContext context, OverrideValue value) => AddContext(context, (Sprite) value.ObjectValue);
    }
}
