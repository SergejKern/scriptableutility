using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility.OverrideValueConverter;


namespace ScriptableUtility.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/Vector3", fileName = nameof(Vector3Variable))]
    public class Vector3Variable : ScriptableVariable<Vector3>, IOverrideMapping
    {
        public void AddContext(IContext context, OverrideValue value) 
            => AddContext(context, Vector3OverrideValueConverter.Get(value));
    }
}
