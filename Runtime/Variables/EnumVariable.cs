using Core.Unity.Attributes;
using UnityEngine;
using Core.Unity.Types;
using ScriptableUtility.OverrideValueConverter;


namespace ScriptableUtility.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/Enum", fileName = nameof(EnumVariable))]
    public class EnumVariable : ScriptableVariable<SerializedEnum>, IOverrideMapping
    {
        public void AddContext(IContext context, OverrideValue value) 
            => AddContext(context, EnumOverrideValueConverter.Get(value));
    }
}
