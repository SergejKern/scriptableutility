﻿using UnityEngine;
using JetBrains.Annotations;

namespace ScriptableUtility.OverrideValueConverter
{
    [UsedImplicitly]
    public class ColorOverrideValueConverter : OverrideValueConverter<Color>
    {
        public static Color Get(OverrideValue value) 
            => !ColorUtility.TryParseHtmlString(value.StringValue, out var color) 
                ? default 
                : color;
        
        public override Color GetValue(OverrideValue value) => Get(value);
        public override OverrideValue GetValue(Color value)
            => new OverrideValue(){ StringValue = ColorUtility.ToHtmlStringRGBA(value) };
    }
}