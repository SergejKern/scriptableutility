﻿using JetBrains.Annotations;
using UnityEngine;

namespace ScriptableUtility.OverrideValueConverter
{
    [UsedImplicitly]
    public class Vector3OverrideValueConverter : OverrideValueConverter<Vector3>
    {
        public static Vector3 Get(OverrideValue value)
        {
            var vars = value.StringValue.Split(',');
            return vars.Length < 3 ? default : new Vector3(float.Parse(vars[0]), float.Parse(vars[1]), float.Parse(vars[2]));
        }

        public override Vector3 GetValue(OverrideValue value) => Get(value);
        public override OverrideValue GetValue(Vector3 value) => new OverrideValue() { StringValue = $"{value.x},{value.y},{value.z}" };
    }
}