﻿using Core.Extensions;
using JetBrains.Annotations;

namespace ScriptableUtility.OverrideValueConverter
{
    [UsedImplicitly]
    public class IntOverrideValueConverter : OverrideValueConverter<int>
    {
        public static int Get(OverrideValue value)
        {
            return value.StringValue.IsNullOrEmpty() 
                ? 0 
                : int.Parse(value.StringValue);
        }

        public override int GetValue(OverrideValue value) => Get(value);
        public override OverrideValue GetValue(int value)
            => new OverrideValue() {StringValue = value.ToString()};
    }
}