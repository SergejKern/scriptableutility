﻿using JetBrains.Annotations;

namespace ScriptableUtility.OverrideValueConverter
{
    [UsedImplicitly]
    public class StringOverrideValueConverter : OverrideValueConverter<string>
    {
        public static string Get(OverrideValue value) => value.StringValue;

        public override string GetValue(OverrideValue value) => Get(value);
        public override OverrideValue GetValue(string value) => new OverrideValue() { StringValue = value };
    }
}