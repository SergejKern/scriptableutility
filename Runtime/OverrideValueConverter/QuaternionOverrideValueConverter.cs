﻿using JetBrains.Annotations;
using UnityEngine;

namespace ScriptableUtility.OverrideValueConverter
{
    [UsedImplicitly]
    public class QuaternionOverrideValueConverter : OverrideValueConverter<Quaternion>
    {
        public static Quaternion Get(OverrideValue value)
        {
            var vars = value.StringValue.Split(',');
            if (vars.Length < 4)
                return default;
            return new Quaternion(float.Parse(vars[0]), float.Parse(vars[1]), float.Parse(vars[2]),
                float.Parse(vars[3]));
        }

        public override Quaternion GetValue(OverrideValue value) => Get(value);
        public override OverrideValue GetValue(Quaternion value)
            => new OverrideValue() { StringValue = $"{value.x},{value.y},{value.z}" };
    }
}