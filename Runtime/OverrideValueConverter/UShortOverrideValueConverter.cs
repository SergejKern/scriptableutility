﻿using Core.Extensions;
using JetBrains.Annotations;

namespace ScriptableUtility.OverrideValueConverter
{
    [UsedImplicitly]
    public class UShortOverrideValueConverter : OverrideValueConverter<ushort>
    {
        public static ushort Get(OverrideValue value)
        {
            return value.StringValue.IsNullOrEmpty() 
                ? (ushort) 0 
                : ushort.Parse(value.StringValue);
        }

        public override ushort GetValue(OverrideValue value) => Get(value);
        public override OverrideValue GetValue(ushort value) => new OverrideValue() {StringValue = value.ToString()};
    }
}