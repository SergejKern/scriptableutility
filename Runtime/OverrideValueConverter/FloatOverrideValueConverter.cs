﻿using Core.Extensions;
using JetBrains.Annotations;

namespace ScriptableUtility.OverrideValueConverter
{
    [UsedImplicitly]
    public class FloatOverrideValueConverter : OverrideValueConverter<float>
    {
        public static float Get(OverrideValue value) 
            => value.StringValue.IsNullOrEmpty()
                ? 0
                : float.Parse(value.StringValue);

        public override float GetValue(OverrideValue value) => Get(value);
        public override OverrideValue GetValue(float value)
            => new OverrideValue() { StringValue = value.ToString("G17") };
    }
}