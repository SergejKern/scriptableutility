using ScriptableUtility.Variables;
using UnityEngine;


namespace ScriptableUtility.VariableNodes
{
    public class Vector3Node : VarNode<Vector3> { }
}
