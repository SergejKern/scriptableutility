using ScriptableUtility.Variables;
using UnityEngine;


namespace ScriptableUtility.VariableNodes
{
    public class SpriteNode : VarNode<Sprite> { }
}
