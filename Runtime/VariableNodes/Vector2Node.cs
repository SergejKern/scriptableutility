using ScriptableUtility.Variables;
using UnityEngine;


namespace ScriptableUtility.VariableNodes
{
    public class Vector2Node : VarNode<Vector2> { }
}
