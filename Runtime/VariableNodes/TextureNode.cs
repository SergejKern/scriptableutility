using ScriptableUtility.Variables;
using UnityEngine;


namespace ScriptableUtility.VariableNodes
{
    public class TextureNode : VarNode<Texture> { }
}
