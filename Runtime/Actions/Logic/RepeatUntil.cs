﻿using System.Collections.Generic;
using Core.Interface;
using ScriptableUtility.ActionConfigs.Logic;

namespace ScriptableUtility.Actions.Logic
{
    public class RepeatUntilAction : IUpdatingAction, IStoppableAction, IContainingActions, 
        IDebugAction
    {
        IBaseAction m_action;

        readonly IVar<bool> m_breakCondition;
        readonly IVar<bool> m_breakValue;
        readonly bool m_abortOnBreak;

        public RepeatUntilAction(IVar<bool> breakCond, IVar<bool> value, bool abortOnBreak)
        {
            m_breakCondition = breakCond;
            m_breakValue = value;
            m_abortOnBreak = abortOnBreak;
        }
        public void LinkActions(IBaseAction action) => m_action = action;

        bool Break => m_breakCondition.Value == m_breakValue.Value;

        public bool IsFinished
        {
            get
            {
                if (m_action is IUpdatingAction up)
                    return Break && (m_abortOnBreak || up.IsFinished);
                return Break;
            }
        }

        public void Init()
        {
            if (m_action is IUpdatingAction up)
                up.Init();
        }

        public void Update(float dt)
        {
            if (Break && m_action is IStoppableAction stoppable)
                stoppable.Stop();
            switch (m_action)
            {
                case IUpdatingAction up:
                {
                    if (up.IsFinished && !Break)
                        up.Init();
                    up.Update(dt);
                }
                break;
                case IDefaultAction def: def.Invoke(); 
                    break;
            }
        }

        public void Stop()
        {
            if (!(m_action is IUpdatingAction up) || up.IsFinished)
                return;
            if (m_action is IStoppableAction stoppable)
                stoppable.Stop();
        }

        public IEnumerable<IBaseAction> GetContainedActions()
            => new[]{m_action};


        string m_debugName;
        public void SetDebugName(string debugName) 
            => m_debugName = debugName;
        public void DebugAction(ref string debugString)
        {
            if (m_action == null)
            {
                debugString += $"{nameof(RepeatUntil)} {m_debugName} Action null";
                return;
            }
            debugString += $"{nameof(RepeatUntil)} {m_debugName} {m_action.GetType()} \n";
            if (m_action is IDebugAction da)
                da.DebugAction(ref debugString);
        }
    }
}
