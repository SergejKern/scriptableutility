﻿using System.Collections.Generic;
using Core.Interface;
//using ScriptableUtility.Variables.Reference;

namespace ScriptableUtility.Actions.Logic
{
    public class IntCheckAction : IDefaultAction
        , IUpdatingAction, IStoppableAction, IInstantCompletableAction
        , IContainingActions
    {
        public enum FinishRule
        {
            ActionOnceDone,
            NothingTodo,
            BreakConditionOnly
        }

        public struct Rules
        {
            public bool BreakByVariable;

            public IVar<int> BreakValue;
            public FinishRule FinishRule;

            public bool WaitForIntChange;
            public bool DontUpdateIntCheck;
        }

        public IntCheckAction(IVar<int> intRef, Rules rules)
        {
            m_intReference = intRef;
            m_rules = rules;

            m_lastAction = null;
            m_instantCompleted = false;
        }
        public void LinkActions(IBaseAction[] intCheckActions) => m_actions = intCheckActions;

        readonly IVar<int> m_intReference;

        IBaseAction[] m_actions;

        readonly Rules m_rules;
        IBaseAction m_lastAction;

        int m_intTest;
        bool m_instantCompleted;

        bool BreakByVariable => m_rules.BreakByVariable &&
                                m_intReference.Value == m_rules.BreakValue.Value;
        bool BreakByFinishRule
        {
            get
            {
                if (m_rules.FinishRule == FinishRule.BreakConditionOnly)
                    return false;

                if (m_lastAction is IUpdatingAction up)
                    return up.IsFinished;

                if (m_rules.FinishRule == FinishRule.ActionOnceDone)
                    return m_lastAction != null;
                if (m_rules.FinishRule == FinishRule.NothingTodo)
                    return true;
                return false;
            }
        }

        public bool IsFinished => BreakByVariable || BreakByFinishRule || m_instantCompleted;
        IBaseAction GetAction(int idx) => m_actions[idx];

        public void Invoke() => (GetAction(m_intReference.Value) as IDefaultAction)?.Invoke();

        public void Init()
        {
            m_intTest = m_intReference.Value;

            if (m_rules.WaitForIntChange)
                return;
            OnIntChanged();
        }

        public void Update(float dt)
        {
            if (m_lastAction is IUpdatingAction up && !up.IsFinished)
                up.Update(dt);

            if (m_rules.DontUpdateIntCheck || IsFinished)
                return;
            if (m_intTest != m_intReference.Value)
                OnIntChanged();
        }
        void OnIntChanged()
        {
            if (m_lastAction is IStoppableAction stop)
                stop.Stop();

            m_lastAction = GetAction(m_intReference.Value);

            if (m_lastAction is IUpdatingAction up)
                up.Init();
            else if (m_lastAction is IDefaultAction def)
                def.Invoke();

            m_intTest = m_intReference.Value;
        }

        public void Stop()
        {
            if (!(m_lastAction is IUpdatingAction up) || up.IsFinished)
                return;
            if (up is IStoppableAction stoppable)
                stoppable.Stop();
        }

        public void InstantComplete() => m_instantCompleted = true;
        public IEnumerable<IBaseAction> GetContainedActions() => m_actions;
    }
}
