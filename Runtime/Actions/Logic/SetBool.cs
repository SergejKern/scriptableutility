using Core.Interface;


namespace ScriptableUtility.Actions.Logic
{
    public class SetBoolAction : IDefaultAction
    {
        public SetBoolAction(IVar<bool> var, IVar<bool> val)
        {
            m_var = var;
            m_val = val;
        }

        readonly IVar<bool> m_var;
        readonly IVar<bool> m_val;

        public void Invoke() => m_var.Value = m_val.Value;
    }
}