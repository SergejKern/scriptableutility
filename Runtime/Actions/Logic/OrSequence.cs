﻿namespace ScriptableUtility.Actions.Logic
{
    public class ORSequenceAction : IBoolAction
    {
        readonly IBoolAction[] m_actions;
        bool m_result;

        public ORSequenceAction(IBoolAction[] actions)
        {
            m_actions = actions;
        }

        public void Invoke()
        {
            m_result = false;
            foreach (var a in m_actions)
            {
                a.Invoke(out m_result);
                if (m_result)
                    break;
            }
        }

        public void Invoke(out bool result)
        {
            Invoke();
            result = m_result;
        }
    }
}
