﻿using Core.Interface;
using Core.Unity.Types;

using GO = UnityEngine.GameObject;

namespace ScriptableUtility.Actions.Logic
{
    public class PropertyToBoolAction : IDefaultAction
    {
        readonly IVar<GO> m_gameObjectRef;
        readonly IVar<bool> m_boolRef;

        readonly ClassTypeReference m_behaviour;
        readonly string m_propertyName;

        public PropertyToBoolAction(IVar<GO> gameObjectRef, IVar<bool> boolRef,
            ClassTypeReference behaviour, string propertyName)
        {
            m_gameObjectRef = gameObjectRef;
            m_boolRef = boolRef;

            m_behaviour = behaviour;
            m_propertyName = propertyName;
        }

        public void Invoke()
        {
            var go = m_gameObjectRef.Value;
            if (go == null)
                return;

            var propertyInfo = m_behaviour.Type.GetProperty(m_propertyName);
            var component = go.GetComponent(m_behaviour.Type);

            if (propertyInfo == null)
                return;
            var test = (bool)propertyInfo.GetMethod.Invoke(component, null);
            m_boolRef.Value = test;
        }
    }
}
