﻿using System.Collections.Generic;

using Core.Interface;


namespace ScriptableUtility.Actions.Logic
{
    public class LoopAction : IDefaultAction, IContainingActions
    {
        readonly IVar<int> m_count;
        readonly IVar<int> m_counter;
        IDefaultAction m_loopAction;

        public LoopAction(IVar<int> count, IVar<int> counter)
        {
            m_count = count;
            m_counter = counter;
        }
        public void LinkActions(IBaseAction loopAction) => m_loopAction = (IDefaultAction) loopAction;

        public void Invoke()
        {
            for (var i = 0; i < m_count.Value; ++i)
            {
                m_counter.Value = i;
                m_loopAction.Invoke();
            }
        }        
        
        public IEnumerable<IBaseAction> GetContainedActions() 
            => new []{m_loopAction};
    }

}
