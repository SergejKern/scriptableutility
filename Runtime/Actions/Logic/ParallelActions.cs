﻿using System.Collections.Generic;
using ScriptableUtility.ActionConfigs;

namespace ScriptableUtility.Actions.Logic
{
    public class ParallelActionsAction : IDefaultAction
        , IUpdatingAction, IStoppableAction, IInstantCompletableAction
        , IContainingActions
        , IDebugAction
    {
        IBaseAction[] m_actions;
        public ParallelActionsAction(){}

        //public ParallelActionsAction(IBaseAction[] actions) => m_actions = actions;
        public void LinkActions(IBaseAction[] actions) => m_actions = actions;

        int m_finishedActions;

        public void Invoke()
        {
            foreach (var a in m_actions)
            {
                if (a is IDefaultAction def)
                    def.Invoke();
            }
        }

        public bool IsFinished => m_finishedActions >= m_actions.Length;
        public void Init()
        {
            m_finishedActions = 0;
            foreach (var action in m_actions)
            {
                if (action is IUpdatingAction up)
                    up.Init();
                else if (action is IDefaultAction def)
                    def.Invoke();
            }
        }

        public void Update(float dt)
        {
            m_finishedActions = 0;
            foreach (var action in m_actions)
            {
                if (!(action is IUpdatingAction up) || up.IsFinished)
                {
                    m_finishedActions++;
                    continue;
                }
                up.Update(dt);
            }
        }

        public void Stop()
        {
            foreach (var action in m_actions)
            {
                if (!(action is IUpdatingAction up) || up.IsFinished)
                    continue;
                if (action is IStoppableAction stop)
                    stop.Stop();
            }
        }

        public IEnumerable<IBaseAction> GetContainedActions() => m_actions;
        public void InstantComplete()
        {
            if (IsFinished)
                return;
            foreach (var a in m_actions)
                if (a is IInstantCompletableAction instant)
                    instant.InstantComplete();
            //else if (a is IDefaultAction def)
            //    def.Invoke();
        }

        string m_debugName;
        public void SetDebugName(string debugName) 
            => m_debugName = debugName;
        public void DebugAction(ref string debugString)
        {
            debugString += $"Parallel {m_debugName} actions finished {m_finishedActions} Unfinished:\n";
            foreach (var action in m_actions)
            {
                if (!(action is IUpdatingAction up) || up.IsFinished)
                    continue;
                if (action is IDebugAction da)
                    da.DebugAction(ref debugString);
                else debugString += $"{action.GetType()} \n";
            }
        }
    }
}
