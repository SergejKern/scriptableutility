﻿using System.Linq;
using Core.Interface;


namespace ScriptableUtility.Actions.Logic
{
    public class ORBoolsAction : IDefaultAction
    {
        readonly IVar<bool>[] m_var;
        readonly bool m_invert;

        readonly IVar<bool> m_result;

        public ORBoolsAction(IVar<bool>[] vars, IVar<bool> result, bool invert)
        {
            m_var = vars;
            m_result = result;
            m_invert = invert;
        }

        public void Invoke()
        {
            var result = m_var.Any(a => a.Value);
            if (m_invert)
                result = !result;
            m_result.Value = result;
        }
    }
}
