﻿using Core.Interface;

namespace ScriptableUtility.Actions.Time
{
    public class WaitAction : IUpdatingAction, IStoppableAction,
        IDebugAction
    {
        readonly IVar<float> m_time;
        float m_timer;
        public WaitAction(IVar<float> time)
        {
            m_time = time;
            m_timer = 0f;
        }

        public bool IsFinished => m_timer >= m_time.Value;

        public void Init() { m_timer = 0f; }

        public void Update(float dt)
        {
            m_timer += dt;
        }

        public void Stop() => m_timer = m_time.Value;


        string m_debugName;
        public void SetDebugName(string debugName) => m_debugName = debugName;
        public void DebugAction(ref string debugString) => debugString += $"Updating {nameof(WaitAction)} {m_debugName} timer {m_timer}\n";
    }

}
