﻿using Core.Interface;


namespace ScriptableUtility.Actions.Math
{
    public class RandomizeIntegerAction : IDefaultAction
    {
        readonly IVar<int> m_variable;
        readonly IVar<int> m_min;
        readonly IVar<int> m_max;

        public RandomizeIntegerAction(IVar<int> variable, IVar<int> min, IVar<int> max)
        {
            m_variable = variable;
            m_min = min;
            m_max = max;
        }

        public void Invoke() => m_variable.Value = UnityEngine.Random.Range(m_min.Value, m_max.Value);
        
    }
}
