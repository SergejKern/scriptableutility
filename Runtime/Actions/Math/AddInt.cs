﻿using Core.Interface;


namespace ScriptableUtility.Actions.Math
{
    public class AddIntAction : IDefaultAction
    {
        readonly IVar<int> m_variable;
        readonly IVar<int> m_value;

        public AddIntAction(IVar<int> variable, IVar<int> val)
        {
            m_variable = variable;
            m_value = val;
        }

        public void Invoke()
        {
            m_variable.Value += m_value.Value;
        }
    }
}
