﻿using Core.Interface;
using ScriptableUtility.Variables;
using UnityEngine;

using GO = UnityEngine.GameObject;

namespace ScriptableUtility.Actions.Mecanim
{
    public class AnimatorCrossFadeAction : IDefaultAction
    {
        readonly IVar<GO> m_gameObject;
        readonly IVar<string> m_stateName;
        readonly IVar<float> m_transitionDuration;
        readonly IVar<int> m_layer;
        readonly IVar<float> m_normalizedTime;

        public AnimatorCrossFadeAction(IVar<GO> gameObject,
            IVar<string> stateName,
            IVar<float> transitionDuration,
            IVar<int> layer,
            IVar<float> normalizedTime)
        {
            m_gameObject = gameObject;
            m_stateName = stateName;
            m_transitionDuration = transitionDuration;
            m_layer = layer;
            m_normalizedTime = normalizedTime;
        }

        public void Invoke()
        {
            var go = m_gameObject.Value;
            var animator = go.GetComponent<Animator>();
            if (animator == null)
                return;

            var layer = -1;
            var normalizedTime = Mathf.NegativeInfinity;

            if (!m_layer.IsNone())
                layer = m_layer.Value;
            if (!m_normalizedTime.IsNone())
                normalizedTime = m_normalizedTime.Value;

            //Debug.Log($"CrossFade: {m_stateName.Value} {m_transitionDuration.Value} {layer} {normalizedTime}");
            animator.CrossFade(m_stateName.Value, m_transitionDuration.Value, layer, normalizedTime);
        }
    }
}