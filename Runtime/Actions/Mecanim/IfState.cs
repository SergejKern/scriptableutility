﻿using System.Collections.Generic;

using Core.Interface;
using UnityEngine;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.Actions.Mecanim
{
    public class IfStateAction : IDefaultAction, IContainingActions
    {
        readonly IVar<GO> m_gameObject;
        readonly IVar<int> m_layerIndex;
        readonly IVar<string> m_stateName;

        readonly IVar<bool> m_isMatching;
        IDefaultAction m_matchAction;
        IDefaultAction m_noMatchAction;

        readonly bool m_checkNextInfoDoNotMatchAsWell;

        // ReSharper disable once TooManyDependencies
        public IfStateAction(IVar<GO> gameObject,
            IVar<int> layerIndex,
            IVar<string> stateName,
            IVar<bool> isMatching,
            bool checkNextInfoDoNotMatchAsWell)
        {
            m_gameObject = gameObject;
            m_layerIndex = layerIndex;
            m_stateName = stateName;
            m_isMatching = isMatching;

            m_checkNextInfoDoNotMatchAsWell = checkNextInfoDoNotMatchAsWell;
        }
        public void LinkActions(IDefaultAction matchAction, IDefaultAction noMatchAction)
        {
            m_matchAction = matchAction;
            m_noMatchAction = noMatchAction;
        }

        public void Invoke()
        {
            var go = m_gameObject.Value;
            if (go == null)
            {
                Debug.Log("No m_gameObject");
                return;
            }

            var anim = go.GetComponent<Animator>();

            if (anim == null)
            {
                Debug.Log("No Animator");
                return;
            }

            var info = anim.GetCurrentAnimatorStateInfo(m_layerIndex.Value);
            var nextInfo = anim.GetNextAnimatorStateInfo(m_layerIndex.Value);

            if (!m_isMatching.IsNone())
                m_isMatching.Value = info.IsName(m_stateName.Value);
            
            //Debug.Log(m_stateName.Value+" "+_info.fullPathHash);
            if (info.IsName(m_stateName.Value))
                m_matchAction?.Invoke();
            else if (!m_checkNextInfoDoNotMatchAsWell || !nextInfo.IsName(m_stateName.Value))
                m_noMatchAction?.Invoke();
        }

        public IEnumerable<IBaseAction> GetContainedActions()
            => new[]{ m_matchAction, m_noMatchAction };
    }

}
