using Core.Interface;
using Core.Unity.Types;
using UnityEngine;

using GO = UnityEngine.GameObject;

namespace ScriptableUtility.Actions.Mecanim
{
    public class TriggerAction : IDefaultAction
    {
        readonly IVar<GO> m_animatorReference;
        readonly AnimatorTriggerRef m_triggerRef;

        public TriggerAction(IVar<GO> animatorReference, AnimatorTriggerRef triggerRef)
        {
            m_animatorReference = animatorReference;
            m_triggerRef = triggerRef;
        }

        public void Invoke()
        {
            var animator = m_animatorReference.Value.GetComponent<Animator>();
            animator.SetTrigger(m_triggerRef.ParameterName);
        }
    }
}
