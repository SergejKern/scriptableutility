﻿using Core.Interface;
using Core.Unity.Types;
using UnityEngine;

using GO = UnityEngine.GameObject;

namespace ScriptableUtility.Actions.Mecanim
{
    public class SetAnimatorFloatAction : IDefaultAction
    {
        readonly IVar<GO> m_gameObject;
        readonly AnimatorFloatRef m_parameter;
        readonly IVar<float> m_value;

        public SetAnimatorFloatAction(IVar<GO> go, AnimatorFloatRef param, IVar<float> val)
        {
            m_gameObject = go;
            m_parameter = param;
            m_value = val;
        }

        public void Invoke()
        {
            var animator = m_gameObject.Value.GetComponent<Animator>();
            animator.SetFloat(m_parameter.ParameterHash, m_value.Value);
        }
    }
}
