﻿using Core.Interface;
using ScriptableUtility.Variables;
using UnityEngine;

using GO = UnityEngine.GameObject;

namespace ScriptableUtility.Actions.Rigidbody
{
    public class AddForceAction : IDefaultAction
    {
        readonly IVar<GO> m_rigidBodyObject;
        readonly IVar<Vector3> m_atPosition;
        readonly IVar<Vector3> m_vector;
        readonly IVar<float> m_x;
        readonly IVar<float> m_y;
        readonly IVar<float> m_z;
        readonly Space m_space;
        readonly ForceMode m_forceMode;

        // ReSharper disable once TooManyDependencies
        public AddForceAction(IVar<GO> rigidbodyObject,
            IVar<Vector3> atPosition,
            IVar<Vector3> vector,
            IVar<float> x,
            IVar<float> y,
            IVar<float> z,
            Space space,
            ForceMode forceMode)
        {
            m_rigidBodyObject = rigidbodyObject;
            m_atPosition = atPosition;
            m_vector = vector;
            m_x = x;
            m_y = y;
            m_z = z;
            m_space = space;
            m_forceMode = forceMode;
        }

        public void Invoke()
        {
            var go = m_rigidBodyObject.Value;
            if (go == null)
                return;
            var rigidBody = go.GetComponent<UnityEngine.Rigidbody>();
            if (rigidBody == null)
                return;

            var force = m_vector.IsNone() ? new Vector3() : m_vector.Value;
            if (!m_x.IsNone()) force.x = m_x.Value;
            if (!m_y.IsNone()) force.y = m_y.Value;
            if (!m_z.IsNone()) force.z = m_z.Value;

            if (m_space == Space.World)
            {
                if (!m_atPosition.IsNone())
                    rigidBody.AddForceAtPosition(force, m_atPosition.Value, m_forceMode);
                else
                    rigidBody.AddForce(force, m_forceMode);
            }
            else rigidBody.AddRelativeForce(force, m_forceMode);
        }
    }
}