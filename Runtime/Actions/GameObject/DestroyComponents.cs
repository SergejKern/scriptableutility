﻿using Core.Interface;
using Core.Unity.Interface;
using Object = UnityEngine.Object;

namespace ScriptableUtility.Actions.GameObject
{
    public class DestroyComponentsAction : IDefaultAction
    {
        readonly IVar<Object>[] m_components;

        public DestroyComponentsAction(IVar<Object>[] components) => 
            m_components = components;

        public void Invoke()
        {
            foreach (var c in m_components)
                Object.Destroy(c.Value);
        }
    }
}
