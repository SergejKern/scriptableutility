﻿using Core.Interface;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.Actions.GameObject
{
    public class ParentAction : IDefaultAction
    {
        readonly IVar<GO> m_newParent;
        readonly IVar<GO> m_child;

        public ParentAction(IVar<GO> newParent, IVar<GO> child)
        {
            m_newParent = newParent;
            m_child = child;
        }

        public void Invoke()
        {
            var parentTr = m_newParent.Value.transform;
            var childTr = m_child.Value.transform;

            childTr.SetParent(parentTr);
        }
    }
}
