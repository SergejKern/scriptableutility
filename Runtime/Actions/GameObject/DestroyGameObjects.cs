﻿using Core.Interface;
using Core.Unity.Extensions;

using GO = UnityEngine.GameObject;

namespace ScriptableUtility.Actions.GameObject
{
    public class DestroyGameObjectsAction : IDefaultAction
    {
        readonly IVar<GO>[] m_gameObjects;

        public DestroyGameObjectsAction(IVar<GO>[] gameObjects)
            => m_gameObjects = gameObjects;

        public void Invoke()
        {
            foreach (var g in m_gameObjects)
                g.Value.TryDespawnOrDestroy();
        }
    }
}
