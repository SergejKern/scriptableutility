﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Interface;
using Core.Unity.Attributes;
using Core.Unity.Types;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace ScriptableUtility.Behaviour
{
    public class Old_FSMActionBehaviour : StateMachineBehaviour
    {
        public List<OldFSMAction> Actions = new List<OldFSMAction>();
        public List<OldFSMUpdateAction> UpdateActions = new List<OldFSMUpdateAction>();
        public List<OldFSMExitAction> ExitActions = new List<OldFSMExitAction>();

        IBaseAction[] m_actions;
        IBaseAction[] m_updateActions;
        IBaseAction[] m_exitActions;

        [AnimTypeDrawer("Finished", hideAnimator: true)]
        [SerializeField] internal AnimatorTriggerRef m_finishTrigger;

        bool m_initialized;

#if UNITY_EDITOR
        public AnimatorTriggerRef MFinishTrigger
        {
            get => m_finishTrigger;
            set => m_finishTrigger = value;
        }

#endif

        void Init(IProvider<IContext> context)
        {
            InitActions(context, Actions, out m_actions);
            InitActions(context, UpdateActions, out m_updateActions);
            InitActions(context, ExitActions, out m_exitActions);

            m_initialized = true;
        }

        static void InitActions<T>(IProvider<IContext> ctx, IReadOnlyList<T> actionData, out IBaseAction[] actions) where T : IOldFSMActionData
        {
            actions = new IBaseAction[actionData.Count];
            for (var i = 0; i < actionData.Count; i++)
            {
                var a = actionData[i];
                actions[i] = a.Init(ctx);
            }
        }

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!m_initialized)
                Init(animator.GetComponent<IProvider<IContext>>());

            foreach (var a in m_actions)
                (a as IDefaultAction)?.Invoke();

            for (var i = 0; i < UpdateActions.Count; i++)
            {
                var actionInfo = UpdateActions[i];
                var action = m_updateActions[i];

                (action as IUpdatingAction)?.Init();

                // todo 1: maybe this is too special case?
                if (!actionInfo.UpdateOnEnter)
                    continue;

                if (action is IUpdatingAction updateAction)
                {
                    updateAction.Update(UnityEngine.Time.deltaTime);
                }
                else (action as IDefaultAction)?.Invoke();
            }

            if (!UpdateActions.IsNullOrEmpty())
                return;

            animator.SetTrigger(m_finishTrigger);
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            foreach (var a in m_exitActions)
                (a as IDefaultAction)?.Invoke();

            for (var i = 0; i < Actions.Count; i++)
            {
                var a = Actions[i];
                if (a.NoStopOnExit)
                    continue;

                if (m_actions[i] is IStoppableAction stoppableAction)
                    stoppableAction.Stop();
            }

            for (var i = 0; i < UpdateActions.Count; i++)
            {
                var a = UpdateActions[i];
                if (a.NoStopOnExit)
                    continue;

                if (m_updateActions[i] is IStoppableAction revertAction)
                    revertAction.Stop();
            }
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            //Debug.Log("On State Update");
            var runningActions = 0;

            for (var i = 0; i < UpdateActions.Count; i++)
            {
                var actionInfo = UpdateActions[i];
                var action = m_updateActions[i];
                if (actionInfo.NeverFinish)
                    runningActions++;

                if (!(action is IUpdatingAction updateAction))
                {
                    (action as IDefaultAction)?.Invoke();
                    continue;
                }

                if (updateAction.IsFinished)
                    continue;

                updateAction.Update(Time.deltaTime);
                if (!updateAction.IsFinished)
                    runningActions++;
            }

            if (runningActions == 0)
                animator.SetTrigger(m_finishTrigger);
        }

        //public override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        //{
        // Debug.Log("On Attack Move ");
        //}
        //public override void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        //{
        // Debug.Log("On Attack IK ");
        //}
    }

    public interface IOldFSMActionData
    {
        ScriptableBaseAction Config { get; }
        IBaseAction Init(IProvider<IContext> ctx);
    }

    [Serializable]
    public struct OldFSMAction : IOldFSMActionData
    {
        public OldFSMAction(ScriptableBaseAction config)
        {
            m_config = config;
            NoStopOnExit = false;
        }

        [SerializeField] ScriptableBaseAction m_config;
        [ShowIf(nameof(IsStoppable))]
        public bool NoStopOnExit;

        public bool IsStoppable => m_config != null && typeof(IStoppableAction).IsAssignableFrom(m_config.FactoryType);
        // ReSharper disable once ConvertToAutoPropertyWithPrivateSetter
        public ScriptableBaseAction Config => m_config;

        public IBaseAction Init(IProvider<IContext> ctx)
        {
            // todo: context shit
            ctx.Get(out var context);
            return m_config.CreateAction();
        }
    }

    [Serializable]
    public struct OldFSMUpdateAction : IOldFSMActionData
    {
        public OldFSMUpdateAction(ScriptableBaseAction config)
        {
            m_config = config;
            UpdateOnEnter = false;
            NeverFinish = false;
            NoStopOnExit = false;
        }

        [SerializeField] ScriptableBaseAction m_config;

        public bool UpdateOnEnter;
        [ShowIf(nameof(HasNoFinishProp))]
        public bool NeverFinish;
        [ShowIf(nameof(IsStoppable))]
        public bool NoStopOnExit;

        public bool IsStoppable => m_config != null && typeof(IStoppableAction).IsAssignableFrom(m_config.FactoryType);
        public bool HasNoFinishProp => m_config != null && !typeof(IUpdatingAction).IsAssignableFrom(m_config.FactoryType);

        // ReSharper disable once ConvertToAutoPropertyWithPrivateSetter
        public ScriptableBaseAction Config => m_config;
        public IBaseAction Init(IProvider<IContext> ctx)
        {
            ctx.Get(out var context);
            return m_config.CreateAction();
        }
    }

    [Serializable]
    public struct OldFSMExitAction : IOldFSMActionData
    {
        public OldFSMExitAction(ScriptableBaseAction config)
        {
            m_config = config;
        }

        [SerializeField] ScriptableBaseAction m_config;
        // ReSharper disable once ConvertToAutoPropertyWithPrivateSetter
        public ScriptableBaseAction Config => m_config;
        public IBaseAction Init(IProvider<IContext> ctx)
        {
            ctx.Get(out var context);
            return m_config.CreateAction();
        }
    }
}
