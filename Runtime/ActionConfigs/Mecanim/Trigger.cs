using System;
using Core.Unity.Attributes;
using Core.Unity.Types;
using ScriptableUtility.Actions.Mecanim;
using UnityEngine;
using nodegraph.Attributes;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.Mecanim
{
    [EditorIcon("icon-action")]
    public class Trigger : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField, Input] internal VarReference<GO> m_animatorReference;
        [SerializeField] internal AnimatorTriggerRef m_triggerRef;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => $"{nameof(Trigger)}";
        static Type StaticFactoryType => typeof(TriggerAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() =>
            new TriggerAction(m_animatorReference.Init(nameof(m_animatorReference), Node), 
                m_triggerRef);
    }
}
