﻿using System;
using Core.Unity.Attributes;
using ScriptableUtility.Actions.Debugging;
using UnityEngine;
using nodegraph.Attributes;

namespace ScriptableUtility.ActionConfigs.Debugging
{
    [EditorIcon("icon-action")]
    [NodeWidth(300)]
    public class DebugLog : ScriptableBaseAction
    {
        [SerializeField, Input()] internal VarReference<string> m_logString;

        public override string Name => $"{nameof(DebugLog)}";
        static Type StaticFactoryType => typeof(DebugLogAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
            => new DebugLogAction(m_logString.Init(nameof(m_logString), Node));
    }
}
