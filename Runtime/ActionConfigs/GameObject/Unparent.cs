﻿using System;
using Core.Unity.Attributes;
using ScriptableUtility.Actions;
using ScriptableUtility.Actions.GameObject;
using ScriptableUtility.Variables;
using UnityEngine;
using UnityEngine.Serialization;
using nodegraph.Attributes;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.GameObject
{
    [EditorIcon("icon-action")]
    public class Unparent : ScriptableBaseAction
    {
        [SerializeField, Input] internal VarReference<GO> m_unparentObj;

        public override string Name => $"{nameof(Unparent)}";
        static Type StaticFactoryType => typeof(UnparentAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() 
            => new UnparentAction(m_unparentObj.Init(nameof(m_unparentObj), Node));
    }
}
