﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Interface;
using Core.Unity.Attributes;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility.Actions.GameObject;
using UnityEngine;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.GameObject
{
    [EditorIcon("icon-action")]
    public class DestroyGameObjects : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField, NodeConnectorList(IO.Input, typeof(VarReference<GO>), syncData: false)]
        internal List<VarReference<GO>> m_gameObjects;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => $"{nameof(DestroyGameObjects)}";
        static Type StaticFactoryType => typeof(DestroyGameObjectsAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() 
            => new DestroyGameObjectsAction(m_gameObjects.Init(nameof(m_gameObjects), Node));
    }

}
