﻿using System;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using ScriptableUtility.Actions.Math;
using UnityEngine;

using nodegraph.Attributes;

namespace ScriptableUtility.ActionConfigs.Math
{
    [EditorIcon("icon-action")]
    public class AddInt : ScriptableBaseAction, IVerify
    {
        [SerializeField, Input]
        internal VarReference<int> m_variable = new VarReference<int>();
        [SerializeField, Input]
        internal VarReference<int> m_value = new VarReference<int>();

        public override string Name
        {
            get
            {
                if (m_variable.Scriptable == null) 
                    return $"Add Int";
                return m_value.ContextType == ReferenceContextType.Intrinsic 
                    ? $"{m_variable.Scriptable.name} += {m_value.IntrinsicValue}" 
                    : $"Add to {m_variable.Scriptable.name}";
            }
        }

        static Type StaticFactoryType => typeof(AddIntAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() =>
            new AddIntAction(m_variable.Init(nameof(m_variable), Node), 
                m_value.Init(nameof(m_value), Node));

        public void Verify(ref VerificationResult result)
        {
            if (m_variable.IsNone())
                result.Error("variable not set", this);
        }
    }
}