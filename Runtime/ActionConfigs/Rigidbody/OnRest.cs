﻿using System;
using Core.Unity.Attributes;
using ScriptableUtility.Actions;
using ScriptableUtility.Actions.Rigidbody;
using ScriptableUtility.Variables;
using UnityEngine;
using UnityEngine.Serialization;
using nodegraph.Attributes;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.Rigidbody
{
    [EditorIcon("icon-action")]
    public class OnRest : ScriptableBaseAction
    {
        //check: has Rigidbody
        [SerializeField, Input] internal VarReference<GO> m_rigidbodyObject;

        [SerializeField] internal float m_restingWhenVelocityMagnitudeLower = 0.25f;
        [SerializeField] internal float m_rayCastGroundCheckDistance = 0.25f;

        public override string Name => nameof(OnRest);
        static Type StaticFactoryType => typeof(OnRestAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() =>
            new OnRestAction(m_rigidbodyObject.Init(nameof(m_rigidbodyObject), Node),
                m_restingWhenVelocityMagnitudeLower, m_rayCastGroundCheckDistance);
    }
}
