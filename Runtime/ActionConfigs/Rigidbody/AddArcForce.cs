﻿using System;
using Core.Unity.Attributes;
using ScriptableUtility.Actions.Rigidbody;
using UnityEngine;
using nodegraph.Attributes;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.Rigidbody
{
    [EditorIcon("icon-action")]
    public class AddArcForce : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        //check: has Rigidbody
        [SerializeField, Input] internal VarReference<GO> m_rigidbodyObject;
        [SerializeField, Input] internal VarReference<Vector2> m_arcForce;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => nameof(AddArcForce);
        static Type StaticFactoryType => typeof(AddArcForceAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() =>
            new AddArcForceAction(
                m_rigidbodyObject.Init(nameof(m_rigidbodyObject), Node),
                m_arcForce.Init(nameof(m_arcForce), Node));
    }
}
