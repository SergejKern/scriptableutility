using System;
using Core.Unity.Attributes;
using ScriptableUtility.Actions.VFX;
using UnityEngine;
using nodegraph.Attributes;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.VFX
{
    [EditorIcon("icon-action")]
    // [ActionCategory("VFX")]
    // [HutongGames.PlayMaker.Tooltip("Start/Stop emitting trail")]
    public class TrailEmit : ScriptableBaseAction
    {
        // [RequiredField]
        // [CheckForComponent(typeof(TrailRenderer))]
        [SerializeField, Input] internal VarReference<GO> m_renderer;
        public override string Name => $"{nameof(TrailEmit)}";
        static Type StaticFactoryType => typeof(TrailEmitAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() => new TrailEmitAction(m_renderer.Init(nameof(m_renderer), Node));
    }
}
