﻿using System;
using System.Linq;
using Core.Extensions;
using Core.Interface;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using ScriptableUtility.Actions;
using ScriptableUtility.Actions.Logic;
using ScriptableUtility.Variables;
using UnityEngine;
using nodegraph;
using nodegraph.Attributes;

namespace ScriptableUtility.ActionConfigs.Logic
{
    [EditorIcon("icon-action")]
    public class AndBools : ScriptableBaseAction, IVerify
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField, NodeConnectorList(IO.Input, typeof(VarReference<bool>), syncData: false)] 
        internal VarReference<bool>[] m_bool;
        [SerializeField, Input] 
        internal VarReference<bool> m_resultBool;
        [SerializeField] bool m_invertBool;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => nameof(AndBools);
        static Type StaticFactoryType => typeof(ANDBoolsAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            return new ANDBoolsAction(m_bool.Init(nameof(m_bool), Node), 
                m_resultBool.Init(nameof(m_resultBool), Node),
                m_invertBool);
        }

        public void Verify(ref VerificationResult result)
        {
            if (m_bool.IsNullOrEmpty())
                result.Error($"Bool List is null or empty!", this);
            else if(m_bool.Any(b => b.Scriptable == null))
                result.Error($"One of the Bool Variables is not set!", this);

            if (m_resultBool.Scriptable == null)
                result.Error($"Result Bool variable is not set!", this);
        }
    }
}
