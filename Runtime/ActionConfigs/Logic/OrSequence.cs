﻿using System;
using System.Collections.Generic;
using Core.Unity.Attributes;
using nodegraph;
using ScriptableUtility.Actions;
using ScriptableUtility.Actions.Logic;
using ScriptableUtility.Attributes;
using UnityEngine;
using nodegraph.Attributes;

namespace ScriptableUtility.ActionConfigs.Logic
{
    [EditorIcon("icon-action")]
    public class ORSequence : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField, NodeConnectorList(IO.Output, typeof(ScriptableActionConnector))]
        internal List<ScriptableBaseAction> m_actions;
        //[SerializeField] internal ScriptableBool m_bool;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => nameof(ORSequence);
        static Type StaticFactoryType => typeof(ORSequenceAction);
        public override Type FactoryType => StaticFactoryType;
       public override IBaseAction CreateAction()
        {
            var actions = new IBoolAction[m_actions.Count];
            for (var i = 0; i < m_actions.Count; i++)
                actions[i] = m_actions[i].CreateAction() as IBoolAction;
            
            return new ORSequenceAction(actions);
        }
    }
}
