using System.Collections.Generic;
using System.Linq;
using Core.Interface;
using Core.Unity.Attributes;
using UnityEngine;

namespace ScriptableUtility
{
    [EditorIcon("icon-ctx")]
    public class ContextComponent : MonoBehaviour, IContext, 
        IProvider<GameObject>, IProvider<Transform>
    {
        [SerializeField]
        internal ScriptableContainer m_container;

        [SerializeField]
        internal List<ScriptableObject> m_variables = new List<ScriptableObject>();

        [SerializeField]
        internal List<ScriptableObject> m_overrideVariables = new List<ScriptableObject>();
        [SerializeField]
        internal List<OverrideValue> m_overrideValues = new List<OverrideValue>();

        public IEnumerable<IScriptableVariable> Variables => VariableObjs.OfType<IScriptableVariable>();
        public List<ScriptableObject> VariableObjs => m_container != null
            ? m_container.VariableObjs
            : m_variables;

        bool m_initialized;


        public void Init()
        {
            if (m_initialized)
                return;
            AddContextToVariables();
        }

        void Awake() => Init();

        void OnDestroy() => RemoveContextFromVariables();

        void AddContextToVariables()
        {
            foreach (var v in VariableObjs)
            {
                if (!(v is IMultiContextMapping isv))
                {
                    Debug.LogError($"Could not add variable {v} to Context {this}");
                    continue;
                }

                var idx = m_overrideVariables.IndexOf(v);
                if (idx != -1 && v is IOverrideMapping iom)
                    iom.AddContext(this, m_overrideValues[idx]);
                else isv.AddContext(this);
            }

            m_initialized = true;
        }
        void RemoveContextFromVariables()
        {
            foreach (var v in Variables)
            {
                if (v is IMultiContextMapping isv)
                    isv.RemoveContext(this);
            }
        }

        public void Get(out IContext provided) => provided = this;
        public void Get(out GameObject provided) => provided = GameObject;
        public void Get(out Transform provided) => provided = Transform;

        public GameObject GameObject => gameObject;
        public Transform Transform => transform;

#if UNITY_EDITOR
        public ScriptableContainer MContainer
        {
            get => m_container;
            set => m_container = value;
        }

        public List<ScriptableObject> MVariables
        {
            get => m_variables;
            set => m_variables = value;
        }
        public List<ScriptableObject> MOverrideVariables
        {
            get => m_overrideVariables;
            set => m_overrideVariables = value;
        }
        public List<OverrideValue> MOverrideValues
        {
            get => m_overrideValues;
            set => m_overrideValues = value;
        }
#endif
    }
}