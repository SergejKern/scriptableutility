﻿using System;
using nodegraph.Interface;

namespace ScriptableUtility
{
    [Serializable]
    public struct ScriptableActionConnector : INodeConnector<IActionConfig> {}
}