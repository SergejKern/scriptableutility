﻿using System;
using UnityEngine;

namespace ScriptableUtility
{
    [Serializable]
    public struct GUIData
    {
        public GUIData(Color color)
        {
            Icon = null;
            UseColor = true;
            GUIColor = color;
        }

        public bool UseColor;
        public Color GUIColor;
        public Texture2D Icon;

        public static GUIData Default => new GUIData
        {
            UseColor = false,
            GUIColor = Color.white,
            Icon = null
        };
    }
}
