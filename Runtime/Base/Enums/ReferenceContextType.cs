namespace ScriptableUtility
{
    public enum ReferenceContextType
    {
        Scriptable,
        Intrinsic,
    }
}