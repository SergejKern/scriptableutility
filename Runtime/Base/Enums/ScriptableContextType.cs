namespace ScriptableUtility
{
    public enum ScriptableContextType
    {
        ObjectContext,
        GlobalContext,
    }
}