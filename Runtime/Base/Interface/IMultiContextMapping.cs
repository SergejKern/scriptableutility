namespace ScriptableUtility
{
    public interface IMultiContextMapping
    {
        void AddContext(IContext context);
        void RemoveContext(IContext context);
    }
}