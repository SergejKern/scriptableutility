using System;
using Core.Interface;

namespace ScriptableUtility
{
    public interface IContextVariableReference : IProvider<IContext>, IVar
    {
        ReferenceContextType ContextType { get; }
        Type VariableType { get; }

        IProvider<IContext> GetContextProvider();
        void SetContextProvider(IProvider<IContext> c);
    }
}