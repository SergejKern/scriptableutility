﻿using System;
using System.Collections.Generic;
using Core.Unity.Interface;

namespace ScriptableUtility
{
    public interface IActionConfig : IScriptableObject
    {
        string Name { get; }
        // please also implement static StaticFactoryType
        Type FactoryType { get; }
        IBaseAction CreateAction();
    }


    public interface IBaseAction { }
    public interface IDefaultAction : IBaseAction
    {
        void Invoke();
    }
    public interface IBoolAction : IDefaultAction
    {
        void Invoke(out bool result);
    }
    public interface IUpdatingAction : IBaseAction
    {
        bool IsFinished { get; }
        void Init();
        void Update(float dt);
    }

    // todo 5: specify how to stop action, check all actions how they handle this and how to setup cases.
    // - IInstantComplete: instant finish and arriving in result state 
    // - IAbort: immediate aborting action
    // - IStop: telling the action to stop early but update for it to properly finish.
    public interface IStoppableAction : IBaseAction
    {
        void Stop();
    }
    public interface IInstantCompletableAction : IBaseAction
    {
        void InstantComplete();
    }
    public interface IContainingActions
    {
        IEnumerable<IBaseAction> GetContainedActions();
    }

    public interface IDebugAction
    {
        void SetDebugName(string debugName);
        void DebugAction(ref string debugString);
    }
}