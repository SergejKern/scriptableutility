namespace ScriptableUtility
{
    public interface IOverrideMapping
    {
        void AddContext(IContext context, OverrideValue value);
    }
}