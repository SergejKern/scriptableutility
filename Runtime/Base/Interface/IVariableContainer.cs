using System.Collections.Generic;
using UnityEngine;

namespace ScriptableUtility
{
    public interface IVariableContainer
    {
        IEnumerable<IScriptableVariable> Variables { get; }

        List<ScriptableObject> VariableObjs { get; }
    }
}