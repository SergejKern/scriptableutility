﻿using System.Collections.Generic;
using Core.Unity.Interface;

namespace ScriptableUtility
{
    public interface ILinkActionConfigs : IDependencies
    {
        IEnumerable<IActionConfig> GetActionConfigsLinked();
        void LinkActions(Dictionary<IActionConfig, IBaseAction> graphActions);
    }
}
