using System;
using Core.Types;
using UnityEditor;
using UnityEngine;

namespace ScriptableUtility
{
    [Serializable]
    public abstract class OverrideValueConverter<T> : ScriptableObject, IOverrideValueConverter
    {
        public static Type ValueType => typeof(T);
        Type IOverrideValueConverter.ValueType => ValueType;


        [SerializeField] public T EditValue;

        SerializedObject m_serializedObject;
        public abstract T GetValue(OverrideValue value);
        public abstract OverrideValue GetValue(T value);

        public virtual ChangeCheck GUI(ref OverrideValue value)
        {
            var currentValue = GetValue(value);

            if (m_serializedObject == null)
            {
                EditValue = currentValue;
                m_serializedObject = new SerializedObject(this);
            }

            if (!Equals(EditValue, currentValue))
            {
                EditValue = currentValue;
                m_serializedObject.Update();
            }

            var prop = m_serializedObject.FindProperty(nameof(EditValue));

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(prop,new GUIContent(""), true);
                if (!check.changed) 
                    return ChangeCheck.NotChanged;

                m_serializedObject.ApplyModifiedProperties();
                value = GetValue(EditValue);
                return ChangeCheck.Changed;
            }
        }
    }

    public interface IOverrideValueConverter
    {
        Type ValueType { get; }
        ChangeCheck GUI(ref OverrideValue value);
    }
}